﻿using Delogi.Form.Model.Main.Class;
using Delogi.Form.Model.Warehouse.Instance.Transaction;
using Delogi.Form.Model.Warehouse.Interface.Transaction;
using Delogi.Form.Model.Warehouse.Class.Transaction;
using Delogi.Form.Model.Warehouse.Instance.Master;
using Delogi.Form.Model.Warehouse.Interface.Master;
using Delogi.Form.Utils;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Telerik.Web.UI;
using System.Threading;
using System.IO;
using System.Web.Services;

namespace Delogi.Form.UI.Warehouse.Transaction
{
    public partial class PartReceivedDetail : System.Web.UI.Page
    {
        GenControl pgc = new GenControl();
        GenMessage pgm = new GenMessage();
        private string tblheader = "dtReceive";
        private string tbldetail = "dtReceiveDet";
        IDtReceive instdtReceive = InstWarehouseDT.GetDtReceive;
        IDtReceiveDet instdtReceiveDet = InstWarehouseDT.GetDtReceiveDet;
        IDtGeneral instGeneral = InstWarehouseDT.GetDtGeneral;
        IMtItems instmtItem = InstWarehouseMT.GetMtItems;
        IMtWerehouse instwhouse = InstWarehouseMT.GetMtWerehouse;
        IMtVariable instvar = InstWarehouseMT.GetMtVariable;
        DtReceive BLReqM = new DtReceive();
        DtReceiveDet BLReqMDet = new DtReceiveDet();
        GenHelper guh = new GenHelper();

        private string classname = "Delogi.Form.UI.Warehouse.Transaction.PartReceivedDetail";

        protected void Page_Load(object sender, EventArgs e)
        {
            bool bolakbalik = false;
            setRegisterControlUpdatePanelTrigger();
            if (!IsPostBack)
            {
                this.GetTableUserOnline();
                this.GetTableMenuDetail();
                this.cekidkiriman();

                BindForm();
                

                bolakbalik = true;
            }
            if(!string.IsNullOrEmpty(hf_ID.Value.Trim()))
            {
                if(!bolakbalik)
                {
                    BindGV(Gv_Detail, getDataTable(false, tbldetail, SetParameter())); 
                }
                //BindForm();
            }
            if(Session["PartReceivedDetailReqmID"] != null)
            {
                if (string.IsNullOrEmpty(hf_ID.Value.Trim()))
                {
                    hf_ID.Value = Session["PartReceivedDetailReqmID"].ToString();
                }
                Btnupdate.Visible = true;
                btnsave.Visible = false;
                Disable();
                Tampil(hf_ID.Value.Trim());  
                Session["PartReceivedDetailReqmID"] = null;
            }
        }

        private void cekidkiriman()
        {

            if (!string.IsNullOrEmpty(hf_tabIndex.Value.Trim()))
            {
                if (!String.IsNullOrEmpty(hf_ID.Value))
                {
                    if (hf_tabIndex.Value == "0")
                    {
                        hf_modeview.Value = "Waiting Generate";
                    }
                    else if (hf_tabIndex.Value == "1")
                    {
                        hf_modeview.Value = "Current";
                    }
                    else
                    {
                        hf_modeview.Value = "History";
                    }
                }


            }
        }

            private void GetTableUserOnline()
        {
            if (Request.QueryString["loginID"] != null)
            {
                string myLoginID = string.Empty;
                myLoginID = Request.QueryString["loginID"].ToString();
                OnlineID = myLoginID;

                UserOnline myLoginData = pgc.GetTableUserOnline(myLoginID);
                hf_loginTime.Value = myLoginData.LoginTime;
                hf_userlevel.Value = myLoginData.UserLevelID;
                hf_userID.Value = myLoginData.UserID;
                hf_empno.Value = myLoginData.EmployeeNo;
                hf_empname.Value = myLoginData.EmployeeName;
                hf_HuroDeptID.Value = myLoginData.DeptID;
                hf_HuroDivisiID.Value = myLoginData.DivisiID;
                hf_menuID.Value = myLoginData.MenuID;
                /*Select IDOnline,SessionID,IPAddress,MenuID,MenuName,MenuUrl,UserLevelID,UserLevelName,AppID,AppName,AppDeptID,AppDeptName,UserID,EmployeeName,EmployeeNo,AccountName,Office,FullName,
                 * Email,DivisiID,DivisiName,DeptID,DeptName,SectionID,SectionName,LineID,LineName,Posisi,LevelEmployee,Status,LoginTime*/
            }


        }
        private string appInID
        {
            get { return hf_ID.Value; }
            set
            {
                hf_ID.Value = value;
            }
        }
        private void GetTableMenuDetail()
        {
            MenuDetail myMenuDetail = pgc.GetTableMenuDetail(OnlineID);
            hf_Add.Value = myMenuDetail.AddData;
            hf_Update.Value = myMenuDetail.UpdateData;
            hf_delete.Value = myMenuDetail.DeleteData;
            hf_ro.Value = myMenuDetail.ROData;
            hf_AllDataBool.Value = myMenuDetail.AllDataBool;
            hf_tabIndex.Value = myMenuDetail.ActiveTab;

            if (!string.IsNullOrEmpty(myMenuDetail.QueryString))
            {
                NameValueCollection qscoll = HttpUtility.ParseQueryString(myMenuDetail.QueryString);
                Hashtable ht = new Hashtable();
                foreach (string item in qscoll)
                {
                    if (item.Trim() == "appInID")
                    {
                        appInID = qscoll[item].ToString().Trim();
                        break;
                    }
                    
                }
            }
            else
            {
                this.ResponseRedirectPage();
            }
        }

        private void ResponseRedirectPage()
        {
            pgc.SetGeneralSearching(OnlineID, hf_AllDataBool.Value, hf_tabIndex.Value);
            Response.Redirect("PartReceived.aspx?&LoginID=" + OnlineID);
        }
        private string OnlineID
        {
            get { return hf_onlineID.Value; }
            set
            {
                hf_onlineID.Value = value;
            }
        }
        #region ENABLE_DISABLE

        private void Disable()
        {
            ddWhouse.Enabled = false;
            TbDoNo.Enabled = false;
            TbDoDate.Enabled = false;
            tbShipNo.Enabled = false;
            ddSupplierCode.Enabled = false;
            tbPONO.Enabled = false;
        }

        private void Enable()
        {
            ddWhouse.Enabled = true;
            TbDoNo.Enabled = true;
            TbDoDate.Enabled = true;
            tbShipNo.Enabled = true;
            ddSupplierCode.Enabled = true;
            tbPONO.Enabled = true;
        }

        #endregion
        #region BIND_FORM

        private void BindForm()
        {
            hf_waktu.Value = DateTime.Now.ToString("HH:mm");
            BindWhouse();
            if (hf_modeview.Value.ToString() == "")
            {
                string year = DateTime.Now.Year.ToString();
                hf_waktu.Value = year;
                hf_stsGV.Value = "1";
                hf_stsBtn.Value = "new";
                //LbDOInputDate.Text = cc.getToday();
                TampilEmp(hf_empno.Value.Trim());
                hf_appstep.Value = "";
            }
            else
            {
                Disable();
                pgc.setControlVisible(false, btnsave);
                pgc.setControlVisible(true, Btnupdate);
                if ((hf_modeview.Value.ToString() == "History") ||
                    (hf_modeview.Value.ToString() == "Current") || (hf_ro.Value.ToString().Trim() == "1") || hf_Update.Value.ToString().Trim() == "0")
                {
                    pgc.setControlVisible(false, btnsave, Btnupdate, BtSubmit);

                }

                Tampil(hf_ID.Value.ToString());
            }

        }

        #endregion

        #region FUNGSI_BIND


        private string textCutDet(string text, int nilai)
        {
            int i = nilai - 1;
            string[] txt = null;

            try
            {
                txt = text.Split(new Char[] { '|' });
                return txt[i];
            }
            catch
            {
                return txt[0];
            }
        }

        private void BindGV(GridView gv, DataTable dt)
        {
            //dt.Columns.Add("No");
            //if (dt.Rows.Count < 1)
            //{
            //    DataRow dr = dt.NewRow();
            //    dr["No"] = "1";

            //    dt.Rows.Add(dr);
            //}

            gv.DataSource = dt;
            gv.DataBind();
            pgc.setGVCaptionTotal(gv, dt.Rows.Count.ToString());
            dt.Dispose();
        }

        //private void BindGV(GridView gv, DataTable dt, string step, string appstep)
        //{
        //    dt.Columns.Add("No");
        //    if (dt.Rows.Count < 1)
        //    {
        //        DataRow dr = dt.NewRow();
        //        dr["No"] = "1";

        //        dt.Rows.Add(dr);
        //    }

        //    //if (!string.IsNullOrEmpty(appstep) || appstep.Trim() != "".Trim())
        //    //{
        //    //    gv.Columns[6].Visible = false;
        //    //    gv.Columns[7].Visible = false;
        //    //}

        //    gv.DataSource = dt;
        //    gv.DataBind();
        //}

        private void BindGVCur(GridView gv, DataTable dt, string step)
        {
            dt.Columns.Add("No");
            if (dt.Rows.Count < 1)
            {
                DataRow dr = dt.NewRow();
                dr["No"] = "1";

                dt.Rows.Add(dr);
            }
            gv.DataSource = dt;
            gv.DataBind();


        }


        private void BindGVAcc(GridView gv, DataTable dt, string apptaskstep)
        {
            dt.Columns.Add("No");
            if (dt.Rows.Count < 1)
            {
                DataRow dr = dt.NewRow();
                dr["No"] = "1";

                dt.Rows.Add(dr);
            }

            if (apptaskstep.Trim() == "1")
            {
                gv.Columns[15].Visible = false;
            }
            else if (apptaskstep.Trim() == "2")
            {
                gv.Columns[15].Visible = true;
            }
            //else if ((step.Trim() == "2") || (step.Trim() == "4"))
            //{
            //    gv.Columns[2].Visible = true;
            //    gv.Columns[3].Visible = false;
            //}

            gv.DataSource = dt;
            gv.DataBind();


        }

        private void BindHistory(GridView gv, DataTable dt)
        {
            gv.DataSource = dt;
            gv.DataBind();
        }

        private void BindDropDown(DataTable pr, DropDownList dd, string name, string id)
        {
            pgc.setControlClear(dd);

            dd.DataSource = pr;
            dd.DataTextField = name;
            dd.DataValueField = id;
            dd.DataBind();
            dd.Items.Insert(0, " ");

        }

        private void bindDropDownnull(DataTable dttemp, string dataText, string valueText, DropDownList dd)
        {
            dd.DataSource = dttemp;
            dd.DataTextField = dataText;
            dd.DataValueField = valueText;
            dd.DataBind();
            ListItem litem = new ListItem();
            litem.Text = "";
            litem.Value = "0";
            dd.Items.Insert(0, litem);
        }

        private void BindDropDownApp(DataTable pr, DropDownList dd, string name, string id)
        {
            pgc.setControlClear(dd);

            dd.DataSource = pr;
            dd.DataTextField = name;
            dd.DataValueField = id;
            dd.DataBind();
            //dd.Items.Insert(0, " ");

        }

        private void BindListbox(DataTable pr, ListBox lb, string nm, string ide)
        {
            pgc.setControlClear(lb);

            lb.DataSource = pr;
            lb.DataTextField = nm;
            lb.DataValueField = ide;
            lb.DataBind();
            //dd.Items.Insert(0, " ");

        }

        private string[] SetArrayStringDet(string TextForArray)
        {
            string[] arrString = TextForArray.Split('|');
            return arrString;
        }

        public string GetListBoxListValue2(ListBox _lb)
        {
            string result = string.Empty;
            int count = 0;
            count = _lb.Items.Count;
            if (count > 0)
            {
                for (int i = 0; i <= count - 1; i++)
                {
                    result += _lb.Items[i].Value.Trim();
                    if (i + 1 != count)
                    {
                        result += "|";
                    }
                }
            }
            _lb.Dispose();
            return result;
        }

        private DataTable getDataTable(bool search, string tblName, GenClassCommonParamList pcp)
        {
            DataTable dtTemp = new DataTable();
            try
            {
                if (tblName == tblheader)
                {
                    dtTemp = instdtReceive.GetDtReceive(pcp);
                }
                else if (tblName == tbldetail)
                {
                    dtTemp = instdtReceiveDet.GetDtReceiveDet(pcp);
                }

                else
                {
                    dtTemp = (DataTable)Session[classname + hf_loginTime.Value.Trim() + tblName];
                    if (dtTemp.Rows.Count < 1)
                    {
                        this.getDataTable(true, tblName, pcp);
                    }
                }
            }
            catch (Exception ex)
            {

                //this.////PGL.(ex);
            }
            finally
            {
                dtTemp.Dispose();

            }
            if (dtTemp.Rows.Count < 1)
            {
                dtTemp.Rows.Add();
            }

            return dtTemp;
        }

        #endregion

        #region MAIN

        private GenClassCommonParamList SetParameter()
        {
            GenClassCommonParamList item = new GenClassCommonParamList();

            item.ParamField1 = "a.ReqmID";
            item.Operrator1 = "=";
            item.ParamFieldData1 = hf_ID.Value.Trim();

            return item;
        }

        private void Tampil(string ID)
        {
            DataTable dttemp = getDataTable(true, tblheader, SetParameter());
            if (dttemp.Rows.Count > 0)
            {
                LbEmpNo.Text = dttemp.Rows[0]["employee_no"].ToString();
                LbEmpName.Text = dttemp.Rows[0]["employee_name"].ToString();
                LbRefno.Text = hf_menuID.Value.Trim() + "-" + dttemp.Rows[0]["ReqmID"].ToString();
                ddWhouse.SelectedValue = dttemp.Rows[0]["whouseID"].ToString();
                TbDoNo.Text = dttemp.Rows[0]["DO_No"].ToString();
                TbDoDate.Text = dttemp.Rows[0]["DO_Date"].ToString();
                tbShipNo.Text = dttemp.Rows[0]["shipNo"].ToString();
                LbDOInputDate.Text = dttemp.Rows[0]["req_Date"].ToString();
                //tbSupplierCode.Text = dttemp.Rows[0]["suppCode"].ToString();
                ddSupplierCode.SelectedValue = dttemp.Rows[0]["SupplierID"].ToString();
                lbSuppName.Text = dttemp.Rows[0]["suppName"].ToString();
                lbSuppAddress.Text = dttemp.Rows[0]["suppAddress"].ToString();
                tbPONO.Text = dttemp.Rows[0]["PONO"].ToString();
                lbPODate.Text = dttemp.Rows[0]["po_date"].ToString();
                lblPacking.Text = dttemp.Rows[0]["packing"].ToString();
                hfSuppID.Value = dttemp.Rows[0]["suppID"].ToString();
                BindGV(Gv_Detail, getDataTable(false, tbldetail, SetParameter()));
            }
        }


        private void TampilEmp(string EmpNo)
        {
            //IMtPurpose inst_master = Inst_Clinic_Master.GetMtPurpose;
            //DataTable dtEmp = inst_master.Get_MtEmployee(EmpNo);

            //if (dtEmp.Rows.Count > 0)
            //{
            LbEmpNo.Text = EmpNo;
            LbEmpName.Text = hf_empname.Value.Trim(); ;//dtEmp.Rows[0]["employee_name"].ToString();
                                                       //}
                                                       //else
                                                       //{
                                                       //    pgm.MessageBoxAjaxs(UpdatePanel1, "Data Not Found", false);
                                                       //}
        }

        #endregion

        #region SAVE_UPDATE
        
        private void BindWhouse()
        {
            DataTable dttemp = instwhouse.GetMtWerehouse();
            bindDropDown(dttemp, "whouseName", "whouseID", ddWhouse);

            DataTable dttemp2 = instvar.GetMtVariable(new GenClassCommonParamList {
                ParamField1 = "a.typeGroup",
                Operrator1 = "=",
                ParamFieldData1 = "Vendor"
            });
            bindDropDown(dttemp2, "typeCode", "varID", ddSupplierCode);

        }


        protected void btnsave_Click(object sender, EventArgs e)
        {
            BLReqM.DO_No = pgc.safeString(TbDoNo.Text.Trim());
            BLReqM.DO_Date = pgc.safeString(TbDoDate.Text.Trim());
            BLReqM.Insert_by = hf_empno.Value.ToString().Trim();
            BLReqM.WhouseID = ddWhouse.SelectedValue.Trim();
            BLReqM.ShipNo = pgc.safeString(tbShipNo.Text.Trim());
            BLReqM.SupplierID = ddSupplierCode.SelectedValue.Trim();// hfSuppID.Value;
            BLReqM.PONo = tbPONO.Text.Trim();
            if (hf_stsBtn.Value.ToString().Trim() == "new")
            {
                int save = 0;
                save = instdtReceive.SaveDtReceive(BLReqM);
                if (save > 0)
                {
                    hf_ID.Value = save.ToString();
                    LbRefno.Text = hf_ID.Value.ToString();
                    btnsave.Visible = false;
                    Disable();
                    Tampil(hf_ID.Value.ToString());
                    //BindGV(Gv_Detail, getDataTable(false, tbldetail, SetParameter()));
                    pgm.MessageBoxAjaxs(UpdatePanel1, pgm.msgSaved(), false);
                    pgc.setControlVisible(true, Btnupdate, panelDetail);
                }
                else
                {
                    pgm.MessageBoxAjaxs(UpdatePanel1, pgm.msgSavedFail(), false);
                }
            }
            else if (hf_stsBtn.Value.ToString().Trim() == "update")
            {
                BLReqM.ReqmID = hf_ID.Value.ToString();

                bool up = instdtReceive.UpdateDtReceive(BLReqM);
                if (up == true)
                {
                    pgm.MessageBoxAjaxs(UpdatePanel1, pgm.msgUpdated(), false);
                    Btnupdate.Visible = true;
                    btnsave.Visible = false;
                    Disable();
                }
                else
                {
                    pgm.MessageBoxAjaxs(UpdatePanel1, pgm.msgUpdatedFail(), false);
                }
            }
            //ResponseRedirectPage();
            //Page_Load(sender, e);
            Session["PartReceivedDetailReqmID"] = hf_ID.Value.Trim();
            pgc.SetGeneralSearching(OnlineID, hf_AllDataBool.Value, hf_tabIndex.Value);
            Response.Redirect("PartReceivedDetail.aspx?&LoginID=" + OnlineID);
        }

        protected void Btnupdate_Click(object sender, EventArgs e)
        {
            hf_stsBtn.Value = "update";
            Enable();
            btnsave.Visible = true;
            Btnupdate.Visible = false;
            hfBtnUploadExcel.Value = "";
        }

        #endregion

        #region DETAIL

        protected void Gv_Detail_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                Label lblno = (Label)e.Row.FindControl("lblNo");
                lblno.Text = Convert.ToString(e.Row.RowIndex + 1 + (Gv_Detail.PageIndex * Gv_Detail.PageSize));


                Button btnitemsCode = (Button)e.Row.FindControl("btnitemsCode");
                if ((hf_modeview.Value.ToString() == "History") ||
                    (hf_modeview.Value.ToString() == "Current") || (hf_ro.Value.ToString().Trim() == "1") || (hf_Update.Value.Trim().ToString() == "0"))
                {
                    btnitemsCode.Visible = false;
                    Gv_Detail.Columns[15].Visible = false;
                    Gv_Detail.Columns[16].Visible = false;
                    Gv_Detail.ShowFooter = false;
                }

            }
        }

        protected void TbItemsCode_TextChanged(object sender, EventArgs e)
        {
            BindGV(Gv_Detail, getDataTable(true, tbldetail, SetParameter()));
        }


        protected void Gv_Detail_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            Control source = e.CommandSource as Control;
            GridViewRow row = source.NamingContainer as GridViewRow;
            panelSearch.Visible = false;

            if (e.CommandName == "save")
            {
                Label lbItemsID = (Label)Gv_Detail.FooterRow.FindControl("lbItemsID");
                Label LblSuppID = (Label)Gv_Detail.FooterRow.FindControl("LblSuppID");
                TextBox TbRecQty = (TextBox)Gv_Detail.FooterRow.FindControl("FTbRecQty");
                TextBox TbLotNo = (TextBox)Gv_Detail.FooterRow.FindControl("FTbLotNo");
                TextBox Tbremark = (TextBox)Gv_Detail.FooterRow.FindControl("FTbremark");
                Label LblUnit = (Label)Gv_Detail.FooterRow.FindControl("LblUnit");
                TextBox TbbatchNo = (TextBox)Gv_Detail.FooterRow.FindControl("FTbbatchNo");
                TextBox FTbExpiredDate = (TextBox)Gv_Detail.FooterRow.FindControl("FTbExpiredDate");

                BLReqMDet.ReqmID = hf_ID.Value.ToString();
                BLReqMDet.ItemsID = pgc.safeString(lbItemsID.Text.Trim());
                BLReqMDet.RecQty = pgc.safeString(TbRecQty.Text.Trim());
                BLReqMDet.LotNo = pgc.safeString(TbLotNo.Text.Trim());
                BLReqMDet.Remark = pgc.safeString(Tbremark.Text.Trim());
                BLReqMDet.SuppID = pgc.safeString(LblSuppID.Text.Trim());
                BLReqMDet.BatchNo = pgc.safeString(TbbatchNo.Text.Trim());
                BLReqMDet.ExpiredDate = pgc.safeString(FTbExpiredDate.Text.Trim());
                //BLReqMDet.Unit_price = pgc.safeString(LblUnit.Text.Trim());
                BLReqMDet.Insert_by = hf_empno.Value.ToString().Trim();

                int save = 0;
                save = instdtReceiveDet.SaveDtReceiveDet(BLReqMDet);

                if (save > 0)
                {
                    hf_advDetID.Value = save.ToString();
                    pgm.MessageBoxAjaxs(UpdatePanel1, pgm.msgSaved(), false);
                    BindGV(Gv_Detail, getDataTable(true, tbldetail, SetParameter()));
                    hf_sts.Value = "";
                }
                else
                {
                    pgm.MessageBoxAjaxs(UpdatePanel1, pgm.msgSavedFail(), false);
                }
            }
            else if (e.CommandName == "del")
            {
                hf_advDetID.Value = ((Label)Gv_Detail.Rows[row.RowIndex].FindControl("LblReqmDetID")).Text.ToString();

                List<GenClassIdentity> mylist = new List<GenClassIdentity>();
                GenClassIdentity pID = new GenClassIdentity();
                pID.ID = hf_advDetID.Value.ToString();
                mylist.Add(pID);

                bool d = instdtReceiveDet.DeleteByDtReceiveDetID(hf_empno.Value.ToString(), mylist);
                if (d == true)
                {
                    pgm.MessageBoxAjaxs(UpdatePanel1, pgm.msgDeleted(), false);
                    BindGV(Gv_Detail, getDataTable(true, tbldetail, SetParameter()));
                }
                else
                {
                    pgm.MessageBoxAjaxs(UpdatePanel1, pgm.msgDeletedFail(), false);
                }

            }
            else if (e.CommandName == "ItemSearch")
            {
                panelSearch.Visible = true;
            }
            else if (e.CommandName == "edit")
            {
                Gv_Detail.ShowFooter = false;
            }
        }

        protected void Gv_Detail_RowEditing(object sender, GridViewEditEventArgs e)
        {
            Gv_Detail.EditIndex = e.NewEditIndex;
            BindGV(Gv_Detail, getDataTable(true, tbldetail, SetParameter()));
            hf_sts.Value = "edit";
            hf_Row.Value = (e.NewEditIndex).ToString();
            Gv_Detail.ShowFooter = false;
        }

        protected void Gv_Detail_RowUpdated(object sender, GridViewUpdatedEventArgs e)
        {
            Gv_Detail.EditIndex = -1;
            this.Gv_Detail.ShowFooter = true;
            BindGV(Gv_Detail, getDataTable(true, tbldetail, SetParameter()));
        }

        protected void Gv_Detail_RowUpdating(object sender, GridViewUpdateEventArgs e)
        {
            if (hf_Update.Value == "1")
            {
                string uang = string.Empty;
                Label LblReqmDetID = (Label)Gv_Detail.Rows[e.RowIndex].FindControl("LblReqmDetID");
                Label LblReqmID = (Label)Gv_Detail.Rows[e.RowIndex].FindControl("LblReqmID");
                TextBox TbRecQty = (TextBox)Gv_Detail.Rows[e.RowIndex].FindControl("ETbRecQty");
                TextBox TbLotNo = (TextBox)Gv_Detail.Rows[e.RowIndex].FindControl("ETbLotNo");
                TextBox Tbremark = (TextBox)Gv_Detail.Rows[e.RowIndex].FindControl("ETbremark");
                TextBox TbItemsCode = (TextBox)Gv_Detail.Rows[e.RowIndex].FindControl("TbItemsCode");
                DropDownList ddcurrency = (DropDownList)Gv_Detail.Rows[e.RowIndex].FindControl("Edcurrency");
                Label LblUnit = (Label)Gv_Detail.Rows[e.RowIndex].FindControl("LblUnit");
                Label LblSuppID = (Label)Gv_Detail.Rows[e.RowIndex].FindControl("LblSuppID");
                Label lbItemsID = (Label)Gv_Detail.Rows[e.RowIndex].FindControl("lbItemsID");
                TextBox TbbatchNo = (TextBox)Gv_Detail.Rows[e.RowIndex].FindControl("ETbbatchNo");
                TextBox ETbExpiredDate = (TextBox)Gv_Detail.Rows[e.RowIndex].FindControl("ETbExpiredDate");

                BLReqMDet.ReqmDetID = LblReqmDetID.Text.Trim();
                BLReqMDet.ReqmID = hf_ID.Value.ToString();
                BLReqMDet.ItemsID = pgc.safeString(lbItemsID.Text.Trim());
                BLReqMDet.RecQty = pgc.safeString(TbRecQty.Text.Trim());
                BLReqMDet.LotNo = pgc.safeString(TbLotNo.Text.Trim());
                BLReqMDet.Remark = pgc.safeString(Tbremark.Text.Trim());
                BLReqMDet.SuppID = pgc.safeString(LblSuppID.Text.Trim());
                // BLReqMDet.Unit_price = pgc.safeString(LblUnit.Text.Trim());
                BLReqMDet.Insert_by = hf_empno.Value.ToString().Trim();
                BLReqMDet.BatchNo = pgc.safeString(TbbatchNo.Text.Trim());
                BLReqMDet.ExpiredDate = pgc.safeString(ETbExpiredDate.Text.Trim());

                bool update = instdtReceiveDet.UpdateDtReceiveDet(BLReqMDet);

                if (update)
                {
                    pgm.MessageBoxAjaxs(UpdatePanel1, pgm.msgUpdated(), false);

                    Gv_Detail.EditIndex = -1;
                    this.Gv_Detail.ShowFooter = true;
                    BindGV(Gv_Detail, getDataTable(true, tbldetail, SetParameter()));
                    hf_sts.Value = "";
                    Gv_Detail.ShowFooter = true;
                }
                else
                {
                    pgm.MessageBoxAjaxs(UpdatePanel1, pgm.msgUpdatedFail(), false);
                }

            }
        }

        protected void Gv_Detail_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
        {
            Gv_Detail.EditIndex = -1;
            this.Gv_Detail.ShowFooter = true;
            BindGV(Gv_Detail, getDataTable(true, tbldetail, SetParameter()));
            hf_sts.Value = "";
            Gv_Detail.ShowFooter = true;
        }

        protected void Gv_Detail_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            this.setPaging(Gv_Detail, sender, e, this.getDataTable(true, tbldetail, SetParameter()));
        }

        protected void Gv_Detail_Sorting(object sender, GridViewSortEventArgs e)
        {
            setSorting(Gv_Detail, "tbldetail", e, getDataTable(true, tbldetail, SetParameter()));
        }
        #endregion

        protected void btnSearch_Click(object sender, EventArgs e)
        {
            if (ddWhouse.SelectedValue != "")
            {
                DataTable dttemp = new DataTable();
                dttemp = instmtItem.GetMtItems(new GenClassCommonParamList
                {
                    ParamField1 = "a.whouseID",
                    Operrator1 = "=",
                    ParamFieldData1 = ddWhouse.SelectedValue
                    ,
                    ParamField2 = ddSearch.SelectedValue,
                    Operrator2 = "like"
                    ,
                    ParamFieldData2 = tbSearch.Text.Trim(),
                    StepName = "LIST_REC"
                });
                BindGV(GVGoodslabelSearch, dttemp);
            }
            else
            {
                pgm.MessageBoxAjaxs(UpdatePanel1, "Please choose Werehouse field", false);
            }
        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            panelSearch.Visible = false;
        }

        #region GvGoods
        

        protected void GVGoodslabelSearch_RowCommand(object sender, GridViewCommandEventArgs e)
        {

            Control source = e.CommandSource as Control;
            GridViewRow row = source.NamingContainer as GridViewRow;

            if (e.CommandName == "addGoods")
            {
                int vindeks = 0;
                vindeks = Convert.ToInt32(row.RowIndex.ToString().Trim());
                string itemsID = ((Label)GVGoodslabelSearch.Rows[vindeks].FindControl("lbitemsID")).Text.ToString();
                string itemsCode = ((Label)GVGoodslabelSearch.Rows[vindeks].FindControl("lbitemsCode")).Text.ToString();
                string itemsName = ((Label)GVGoodslabelSearch.Rows[vindeks].FindControl("lbitemsName")).Text.ToString();
                string itemsDesc = ((Label)GVGoodslabelSearch.Rows[vindeks].FindControl("lbitemDesc")).Text.ToString();
                string goodsUnit = ((Label)GVGoodslabelSearch.Rows[vindeks].FindControl("lbgoodsUnit")).Text.ToString();
                string suppname = ((Label)GVGoodslabelSearch.Rows[vindeks].FindControl("lblsuppName")).Text.ToString();
                string suppID = ((Label)GVGoodslabelSearch.Rows[vindeks].FindControl("LblSuppID")).Text.ToString();
                string SBQ = ((Label)GVGoodslabelSearch.Rows[vindeks].FindControl("LbSBQ")).Text.ToString();
                string labelType = ((Label)GVGoodslabelSearch.Rows[vindeks].FindControl("lblabelType")).Text.ToString();

                if (hf_sts.Value.ToString().Trim() == "edit")
                {
                    Label lbitemsID = (Label)Gv_Detail.Rows[Convert.ToInt32(hf_Row.Value.ToString())].FindControl("lbitemsID");
                    TextBox tbitemsCode = (TextBox)Gv_Detail.Rows[Convert.ToInt32(hf_Row.Value.ToString())].FindControl("TbItemsCode");
                    Label lblitemsName = (Label)Gv_Detail.Rows[Convert.ToInt32(hf_Row.Value.ToString())].FindControl("LblitemsName");
                    Label LblitemsNameAlias = (Label)Gv_Detail.Rows[Convert.ToInt32(hf_Row.Value.ToString())].FindControl("LblitemsNameAlias");
                    Label lblgoodsUnit = (Label)Gv_Detail.Rows[Convert.ToInt32(hf_Row.Value.ToString())].FindControl("LblUnit");
                    Label LblSuppName = (Label)Gv_Detail.Rows[Convert.ToInt32(hf_Row.Value.ToString())].FindControl("LblSuppName");
                    Label LblSuppID = (Label)Gv_Detail.Rows[Convert.ToInt32(hf_Row.Value.ToString())].FindControl("LblSuppID");
                    Label LblSBQ = (Label)Gv_Detail.Rows[Convert.ToInt32(hf_Row.Value.ToString())].FindControl("LblSBQ");
                    Label LbllblType = (Label)Gv_Detail.Rows[Convert.ToInt32(hf_Row.Value.ToString())].FindControl("LbllabelType");




                    tbitemsCode.Text = itemsCode;
                    lblitemsName.Text = itemsName;
                    lbitemsID.Text = itemsID;
                    lblgoodsUnit.Text = goodsUnit;
                    LblSuppName.Text = suppname; ;
                    LblSuppID.Text = suppID;
                    LblSBQ.Text = SBQ;
                    LbllblType.Text = labelType;
                    LblitemsNameAlias.Text = itemsDesc;

                    //  lbSupplier.Text = suppname;

                }
                else
                {
                    Label lbitemsID = (Label)Gv_Detail.FooterRow.FindControl("lbItemsID");
                    TextBox tbitemsCode = (TextBox)Gv_Detail.FooterRow.FindControl("TbItemsCode");
                    Label lblitemsName = (Label)Gv_Detail.FooterRow.FindControl("LblitemsName");
                    Label LblitemsNameAlias = (Label)Gv_Detail.FooterRow.FindControl("LblitemsNameAlias");
                    Label lblgoodsUnit = (Label)Gv_Detail.FooterRow.FindControl("LblUnit");
                    Label lblnowBudget = (Label)Gv_Detail.FooterRow.FindControl("LblSuppName");
                    Label LblSuppID = (Label)Gv_Detail.FooterRow.FindControl("LblSuppID");
                    Label LblSBQ = (Label)Gv_Detail.FooterRow.FindControl("LblSBQ");
                    Label LbllabelType = (Label)Gv_Detail.FooterRow.FindControl("LbllabelType");



                    tbitemsCode.Text = itemsCode;
                    lblitemsName.Text = itemsName;
                    lbitemsID.Text = itemsID;
                    lblgoodsUnit.Text = goodsUnit;
                    lblnowBudget.Text = suppname;
                    LblSuppID.Text = suppID;
                    LblSBQ.Text = SBQ;
                    LbllabelType.Text = labelType;
                    LblitemsNameAlias.Text = itemsDesc;

                    // lbSupplier.Text = suppname;GVGoodslabelSearch_RowDataBound
                }
            }
        }

        protected void GVGoodslabelSearch_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                Label lblno = (Label)e.Row.FindControl("lblNo");
                lblno.Text = Convert.ToString(e.Row.RowIndex + 1 + (GVGoodslabelSearch.PageIndex * GVGoodslabelSearch.PageSize));
            }
        }

        protected void GVGoodslabelSearch_Sorting(object sender, GridViewSortEventArgs e)
        {

        }

        protected void GVGoodslabelSearch_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            DataTable dttemp = instmtItem.GetMtItems(new GenClassCommonParamList { ParamField1 = "a.whouseID", Operrator1 = "=", ParamFieldData1 = ddWhouse.SelectedValue, ParamField2 = ddSearch.SelectedValue, Operrator2 = "like", ParamFieldData2 = tbSearch.Text.Trim(), StepName = "LIST_SEARCH" });
            this.setPaging(GVGoodslabelSearch, sender, e, dttemp);
        }
        #endregion

        protected void BtSubmit_Click(object sender, EventArgs e)
        {
            DtGeneral item = new DtGeneral();
            item.InsertBy = hf_empno.Value;
            item.StepName = "GenerateLabel"; //SPNamenya khusus
            item.SPListBool = "0";
            item.ID = hf_ID.Value.ToString();
            item.InsertBy = hf_empno.Value.ToString();
            //int gen = 0; // instReqMDet.GenerateLabel(hf_ID.Value.ToString(), hf_empno.Value.ToString());
            bool gen = instGeneral.UpdateSubmitDtGeneral(item);
            if (gen)//> 0
            {
                pgm.MessageBoxAjaxs(UpdatePanel1, "Generate Label Success !", false);
                SetSession();
                ResponseRedirectPage();
            }
            else
            {
                pgm.MessageBoxAjaxs(UpdatePanel1, "Generate Label Failed !", false);
            }
        }

       
        #region BindData

        private void bindDropDown(DataTable dttemp, string dataText, string valueText, DropDownList dd)
        {
            dd.DataSource = dttemp;
            dd.DataTextField = dataText;
            dd.DataValueField = valueText;
            dd.DataBind();
            ListItem litem = new ListItem();
            litem.Text = "";
            litem.Value = "0";
            dd.Items.Insert(0, litem);
        }


        private void SetCreate()
        {
            string year = DateTime.Now.Year.ToString();
            hf_waktu.Value = year;
            btnsave.Text = "Save";
        }
        private void SetView()
        {


        }

        private void BindDataGV(GridView gv, DataTable dt)
        {
            gv.DataSource = dt;
            gv.DataBind();
            setRegisterControlUpdatePanelTrigger();
            pgc.setGVCaptionTotal(gv, dt.Rows.Count.ToString());
            dt.Dispose();

        }
        

        private GenClassCommonParamList setParameter(string tableName)
        {
            GenClassCommonParamList pcp = new GenClassCommonParamList();

            if (!string.IsNullOrEmpty(hf_ID.Value))
            {
                pcp.ParamField1 = "a.itemsID";
                pcp.Operrator1 = "=";
                pcp.ParamFieldData1 = hf_ID.Value.Trim();
                //pcp.ParamTab = hf_modeView.Value.Trim();
            }
            return pcp;
        }

        private DataTable GetDatatable(bool search, string stepname, GenClassCommonParamList pcp)
        {
            string menuID = hf_menuID.Value;
            DataTable dtTemp = new DataTable();
            string mySession = classname + stepname + hf_empno.Value + hf_loginTime.Value;
            try
            {
                if ((!IsPostBack && !IsExistsSession(mySession)) || search || !IsExistsSession(mySession))
                {
                    //dtTemp = instmtItem.GetMtItems(pcp);
                    this.SetDataSession(dtTemp, mySession);
                }
                else
                {
                    if (Session[mySession] != null)
                    {
                        dtTemp = (DataTable)Session[mySession];
                    }
                    if (dtTemp.Rows.Count < 1)
                    {
                        this.GetDatatable(true, stepname, pcp);
                    }
                }

            }
            catch (Exception ex) { }
            if (dtTemp.Rows.Count == 0)
                dtTemp.Rows.Add();

            return dtTemp;
        }
        

        #endregion BindData

        #region SessionTab
        private void SetSession()
        {
            Session["dept_huro"] = hf_dept.Value;
            Session["divisi"] = hf_divisi.Value;
            Session["ipaddress"] = hf_ipaddress.Value;

            if (Session["LoginTime"] != null)
                Session.Remove("LoginTime");
            Session["LoginTime"] = hf_loginTime.Value.Trim();
            if (Session["user_level_id"] != null)
                Session.Remove("user_level_id");
            Session["user_level_id"] = hf_userlevel.Value.Trim();
            if (Session["user_id"] != null)
                Session.Remove("user_id");
            Session["user_id"] = hf_userID.Value.Trim();
            if (Session["deptid"] != null)
                Session.Remove("deptid");
            Session["deptid"] = hf_deptID.Value.Trim();
            if (Session["deptName"] != null)
                Session.Remove("deptName");
            if (Session["employee_no"] != null)
                Session.Remove("employee_no");
            Session["employee_no"] = hf_empno.Value.Trim();
            if (Session["employee_name"] != null)
                Session.Remove("employee_name");
            Session["employee_name"] = hf_empname.Value.Trim();
            if (Session["menu_ID"] != null)
                Session.Remove("menu_ID");
            Session["menu_ID"] = hf_menuID.Value.Trim();
            if (Session["HURODeptID"] != null)
                Session.Remove("HURODeptID");
            Session["HURODeptID"] = hf_HuroDeptID.Value.Trim();
            if (Session["HUROdivisiID"] != null)
                Session.Remove("HUROdivisiID");
            Session["HUROdivisiID"] = hf_HuroDivisiID.Value.Trim();

        }
        private void SetDataSession(DataTable dtTemp, string sessioName)
        {

            if (Session[sessioName] != null)
            {
                Session.Remove(sessioName);
            }
            Session[sessioName] = dtTemp;
        }
        private void SetStringSession(string strTemp, string sessioName)
        {
            if (Session[sessioName] != null)
            {
                Session.Remove(sessioName);
            }
            Session[sessioName] = strTemp;
        }
        private bool IsExistsSession(string sessionName)
        {
            if (Session[sessionName] != null)
                return true;
            else
                return false;
        }
        public override void VerifyRenderingInServerForm(Control control)
        {
            return;
        }
        private void setRegisterControlUpdatePanelTrigger()
        {
            ScriptManager sm = ScriptManager.GetCurrent(this.Page);
            sm.RegisterPostBackControl(btnClose);
            sm.RegisterPostBackControl(btnUpload);
            sm.RegisterPostBackControl(btnProcess);
        }
        private void GetSession()
        {
            if (Session["LoginTime"] != null)
                hf_loginTime.Value = Session["LoginTime"].ToString();
            if (Session["user_level_id"] != null)
                hf_userlevel.Value = Session["user_level_id"].ToString();
            if (Session["user_id"] != null)
                hf_userID.Value = Session["user_id"].ToString();
            if (Session["deptid"] != null)
                hf_deptID.Value = Session["deptid"].ToString();
            if (Session["employee_no"] != null)
                hf_empno.Value = Session["employee_no"].ToString();
            if (Session["employee_name"] != null)
                hf_empname.Value = Session["employee_name"].ToString();
            if (Session["HURODeptID"] != null)
                hf_HuroDeptID.Value = Session["HURODeptID"].ToString();
            if (Session["HUROdivisiID"] != null)
                hf_HuroDivisiID.Value = Session["HUROdivisiID"].ToString();
            if (Session["menu_ID"] != null)
                hf_menuID.Value = Session["menu_ID"].ToString();
            if (Session["dept_huro"] != null)
                hf_dept.Value = Session["dept_huro"].ToString();
            if (Session["divisi"] != null)
                hf_divisi.Value = Session["divisi"].ToString();
            if (Session["ipaddress"] != null)
                hf_ipaddress.Value = Session["ipaddress"].ToString();

            if (Session["var"] != null)
            {
                //   Tab.ActiveTabIndex = pgc.safeInt(Session["var"].ToString());
                Session.Remove("var");
            }
        }
        private void GetSessionTest()
        {
            hf_loginTime.Value = DateTime.Now.ToString();
            hf_userlevel.Value = "1";
            hf_userID.Value = "388";
            hf_deptID.Value = "1";
            hf_empno.Value = "2160071";
            hf_empname.Value = "Indriani Dewi Safitri";
            hf_HuroDeptID.Value = "4";
            hf_HuroDivisiID.Value = "5";
            hf_menuID.Value = "283";
            hf_dept.Value = "ISD";
            hf_divisi.Value = "PPnC";
        }
        private void SetTabSession(int myActiveTab, string cari, string value, string tanggal, string dari, string sampai, string sessioName, string page)
        {
            try
            {
                if (Session[sessioName] != null)
                {
                    Session.Remove(sessioName);
                }
                Session.Remove(sessioName);
                Session[sessioName] = myActiveTab + "|" + cari + "|" + value + "|" + tanggal + "|" + dari + "|" + sampai + "|" + page;
            }
            catch (Exception ex)
            {
                //this.//PGL.(ex, "SetDataSession");
            }
        }

        private string textCut(string text, int nilai)
        {
            int i = nilai - 1;
            string[] txt = null;

            try
            {
                txt = text.Split(new Char[] { '|' });
                return txt[i];
            }
            catch
            {
                return txt[0];
            }
        }

        #endregion

        

        #region gridviewPage
        private string GridViewSortDirection
        {
            get { return ViewState["SortDirection"] as string ?? "DESC"; }
            set { ViewState["SortDirection"] = value; }
        }
        private string GridViewSortExpression
        {
            get { return ViewState["SortExpression"] as string ?? string.Empty; }
            set { ViewState["SortExpression"] = value; }
        }
        private void setSorting(GridView gv, object sender, GridViewSortEventArgs e, DataTable dtTemp)
        {
            GridViewSortExpression = e.SortExpression;
            this.pgc.GVSortExpression = GridViewSortExpression;
            this.pgc.GVSortDirection = GridViewSortDirection;
            int pageIndex = gv.PageIndex;
            gv.DataSource = this.pgc.SortDataTable(dtTemp, false);
            GridViewSortDirection = this.pgc.GVSortDirection;
            gv.DataBind();
            gv.PageIndex = pageIndex;
        }
        private void setPaging(GridView gv, object sender, GridViewPageEventArgs e, DataTable dtTemp)
        {
            gv.SelectedIndex = -1;
            this.pgc.GVSortExpression = GridViewSortExpression;
            this.pgc.GVSortDirection = GridViewSortDirection;
            gv.DataSource = this.pgc.SortDataTable(dtTemp, true);
            gv.PageIndex = e.NewPageIndex;
            gv.DataBind();

        }
        private void setRowsNum(GridView gv, string numrows, DataTable dtTemp)
        {
            int data = 0;
            int data1 = 0;
            data = this.pgc.safeInt(numrows);
            data1 = dtTemp.Rows.Count;
            if (data > 0)
            {
                if (data1 > 0)
                {
                    if (data1 < data)
                        data = data1;
                    gv.DataSource = pgc.SortDataTable(dtTemp, true);
                    gv.PageSize = data;
                    gv.DataBind();
                }
            }
        }
        private void setPageIndex(GridView gv, string indeks, DataTable dtTemp)
        {
            int indek = 0;
            int data1 = 0;
            int data2 = 0;
            indek = this.pgc.safeInt(indeks);
            if (indek <= 0)
                indek = 1;
            indek = indek - 1;
            data1 = dtTemp.Rows.Count;
            if (data1 > 0)
            {
                data2 = gv.PageCount;
                if (indek < 0 || indek > data2)
                    indek = data2;
                gv.DataSource = pgc.SortDataTable(dtTemp, true);
                gv.PageIndex = indek;
                gv.DataBind();
            }
        }

        #endregion

        #region gridviewEvent

        protected void GV_Sorting(object sender, GridViewSortEventArgs e)
        {
            //this.setSorting(((GridView)sender), sender, e, GetDatatable(false, tblmtItems, setParameter(tblmtItems)));

        }
        protected void GV_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
           // this.setPaging(((GridView)sender), sender, e, GetDatatable(false, tblmtItems, setParameter(tblmtItems)));

        }
        protected void GV_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            GridView gv = ((GridView)sender);
            string GVID = ((GridView)sender).ID.ToString();
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                Label lblNumber = (Label)e.Row.FindControl("lblNo");
                lblNumber.Text = Convert.ToString((e.Row.RowIndex + 1) + (((GridView)sender).PageIndex * ((GridView)sender).PageSize));

                if (hf_tabIndex.Value == "0" || hf_tabIndex.Value == "")
                {
                    Button btnDelete = (Button)e.Row.FindControl("btnDelete");
                    btnDelete.Visible = true;
                }
            }

            if (hf_tabIndex.Value == "0" || hf_tabIndex.Value == "")
            {

                gv.ShowFooter = true;
            }

        }
        protected void GridView_PreRender(object sender, EventArgs e)
        {
            GridView gv = (GridView)sender;
            if ((gv.Rows.Count > 0))
            {
                gv.UseAccessibleHeader = true;
                gv.HeaderRow.TableSection = TableRowSection.TableHeader;
            }
        }

        protected void GV_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            Control source = e.CommandSource as Control;
            GridViewRow row = source.NamingContainer as GridViewRow;

        }
        #endregion
        private bool SaveData(string itemSave)
        {
            bool result = false;

            return result;

        }

        #region SaveData

        #endregion SaveData
        

        protected void btnClose_Click(object sender, EventArgs e)
        {
            //SetSession();
            //SetStringSession(hf_tabIndex.Value, "sesTab" + hf_empno.Value.Trim() + hf_loginTime.Value.Trim() + hf_menuID.Value.Trim());

            //Response.Redirect("CandidateApproval.aspx?",false);
            this.ResponseRedirectPage();

        }

        protected void gvHistoryApproval_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            string GVID = ((GridView)sender).ID.ToString();
            //if (e.Row.RowType == DataControlRowType.DataRow)
            //{
            //    Label lblNumber = (Label)e.Row.FindControl("lblNo");
            //    lblNumber.Text = Convert.ToString((e.Row.RowIndex + 1) + (((GridView)sender).PageIndex * ((GridView)sender).PageSize));

            //}
            if (e.Row.RowType == DataControlRowType.Header)
            { e.Row.TableSection = TableRowSection.TableHeader; }

            if (e.Row.RowType == DataControlRowType.Pager)
            { e.Row.TableSection = TableRowSection.TableFooter; }
            //TableFooterRow tfr = new TableFooterRow();
            //tfr.TableSection = TableRowSection.TableFooter; // ADD THIS LINE

        }

        protected void ScanQR_Click(object sender, EventArgs e)
        {

        }


        #region uploadexcel
        protected void btnUpload_Click(object sender, EventArgs e)
        {
            string myErro = string.Empty;
            if (FileUpload1.HasFile)
            {
                string tempUrl = string.Empty;
                try
                {
                    string XlsPath = Server.MapPath("~/Reports/") + Path.GetFileName(FileUpload1.FileName);
                    FileUpload1.SaveAs(XlsPath);

                    string myUrl = Path.GetFullPath(XlsPath);
                    DataTable dtsheet = guh.ExcelGetSheetList(myUrl);

                    FileUpload1.ToolTip = myUrl;
                    DataTable dtuplo = guh.ExcelToDatatableOpenXML(out myErro, FileUpload1.ToolTip);

                    Session["sessionuplodfile"] = dtuplo;
                    lblFile.Text = "Nama File : " +
                    FileUpload1.PostedFile.FileName + "<br>" +
                    FileUpload1.PostedFile.ContentLength + "kb, " + dtuplo.Rows.Count.ToString() + " Data<br>" +
                    "<b>Data Xlsx berhasil di baca.</b>";

                    gvSalesDet.DataSource = dtuplo;
                    gvSalesDet.DataBind();
                    btnProcess.Enabled = true;

                    //DDSheetName.DataSource = dtsheet;
                    //DDSheetName.DataValueField = "TABLE_NAME";
                    //DDSheetName.DataTextField = "TABLE_NAME";
                    //DDSheetName.DataBind();
                    //DDSheetName_SelectedIndexChanged(sender, e);
                }
                catch (Exception ex)
                {

                    throw;
                }
            }
        }

        protected void btnProcess_Click(object sender, EventArgs e)
        {
            try
            {

                int errCOun = 0;
                //DAODtBON dAODtTransCred = new DAODtBON();
                List<List<string>> Li = new List<List<string>>();
                DataTable dt = (DataTable)Session["sessionuplodfile"];

                DataTable dtRe = new DataTable();
                dtRe.Columns.Add("RangeStart");
                dtRe.Columns.Add("RangeSEnd");
                dtRe.Columns.Add("Result");

                int MyStart = 0;
                int MyInterval = 50;
                int MyLoopCount = 0;
                int MyLast = dt.Rows.Count;
                if (dt.Rows.Count % 50 > 0)
                {
                    MyLoopCount = (dt.Rows.Count / 50) + 1;

                }
                else MyLoopCount = dt.Rows.Count / 50;


                for (int i = 1; i <= MyLoopCount; i++)
                {
                    MyStart = (i - 1) * MyInterval;
                    int limitLoop = 0;
                    if ((MyInterval * i) > MyLast) limitLoop = MyLast;
                    else limitLoop = MyInterval * i;
                    for (int j = MyStart; j < limitLoop; j++)
                    {
                        List<string> cSList = new List<string>();
                        cSList.Add(hf_ID.Value.Trim());
                        for (int k = 0; k < dt.Columns.Count; k++)
                        {
                            cSList.Add(dt.Rows[j][k].ToString());
                        }
                        Li.Add(cSList);
                    }
                    int gh = instdtReceiveDet.SaveDtUpload(Li);
                    if (gh > 0) errCOun++;
                    DataRow dr = dtRe.NewRow();
                    dr["RangeStart"] = MyStart.ToString();
                    dr["RangeSEnd"] = (MyInterval * i).ToString();
                    dr["Result"] = gh.ToString();
                    dtRe.Rows.Add(dr);
                    Li.Clear();

                    string sdasdasdas = string.Empty;
                }
                string pes = string.Empty;
                for (int i = 0; i < dtRe.Rows.Count; i++)
                {
                    pes = dtRe.Rows[i]["RangeStart"].ToString() + " sampai " + dtRe.Rows[i]["RangeSEnd"].ToString() + dtRe.Rows[i]["Result"].ToString() + "\n";
                }
                if (errCOun == 0)
                {
                    Session.Remove("sessionuplodfile");
                    //MessageBox.Show(this.Page, "Data berhasil di upload");
                    pgm.MessageBoxAjaxs(UpdatePanel1, pgm.msgFileUploaded(), false);


                }
                else { Session.Remove("sessionuplodfile"); pgm.MessageBoxAjaxs(UpdatePanel1, pgm.msgFileUploadedFailed(), false); }

                //bindgvSalesDet(lblID.Text);
                BindGV(Gv_Detail, getDataTable(true, tbldetail, SetParameter()));
                btnProcess.Enabled = false;
                lblFile.Text = "";
            }
            catch (Exception ex)
            {
                Session.Remove("sessionuplodfile");
                //MessageBox.Show(this.Page, "Data gagal di upload : " + ex.ToString());
                pgm.MessageBoxAjaxs(UpdatePanel1, pgm.msgFileUploadedFailed(), false);

            }
        }
        #endregion

        #region gv Bind
        private void bindgvSalesDet(string a)
        {
            DataTable dt = GetDatatable(true, tbldetail, setParameter(a));
            GridView gv = new GridView();
            gv = gvSalesDet;
            pgc.BindGV(gv, dt);

        }
        private void bindDd(DataTable dt, string dataText, string valueText, DropDownList dd)
        {
            dd.DataSource = dt;
            dd.DataTextField = dataText;
            dd.DataValueField = valueText;
            dd.DataBind();
            ListItem litem = new ListItem();
            litem.Text = "";
            litem.Value = "0";
            dd.Items.Insert(0, litem);
        }
        #endregion gv Bind

        #region gv Event

        protected void gvSalesDet_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            GridView gv = gvSalesDet;
            //gv.Columns[8].Visible = false;
            //gv.Columns[9].Visible = false;

            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                Label lblNumber = (Label)e.Row.FindControl("lblNo");
                lblNumber.Text = Convert.ToString((e.Row.RowIndex + 1) + (gv.PageIndex * gv.PageSize));
            }
            if (e.Row.RowType == DataControlRowType.Header)
            { e.Row.TableSection = TableRowSection.TableHeader; }

            if (e.Row.RowType == DataControlRowType.Pager)
            { e.Row.TableSection = TableRowSection.TableFooter; }
            
        }
        protected void gvSalesDet_Sorting(object sender, GridViewSortEventArgs e)
        {
            GridView gv = gvSalesDet;
            this.setSorting(((GridView)sender), sender, e, GetDatatable(false, tbldetail, setParameter(tbldetail)));
        }
        protected void gvSalesDet_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            this.setPaging(((GridView)sender), sender, e, GetDatatable(false, tbldetail, setParameter(tbldetail)));
        }
        #endregion gv Event

        [WebMethod()]
        public static string ScanData(string param)
        {
            string myRes =  "[SUCCESS]"; //hf_ID.Value.Trim();
            string[] paramArray = param.Split(',');
            List<List<string>> li = new List<List<string>>();
            //List<string> mylist = new List<string>();
            string myID = paramArray[0];
            string kode2 = paramArray[1];
            string partno = paramArray[42];
            string runningno = paramArray[43]; //running no
            myRes += "|" + paramArray[2];

            string cb = paramArray.Length.ToString();

            for (int i = 4; i < paramArray.Length-10; i++)
            {
                
                // mylist.Add(paramArray[i] + "," + paramArray[i + 1]  + "," + paramArray[i + 2] + "," + paramArray[i + 3]);
                List<string> mylist = new List<string>();
                //mylist.Add(myre);
                mylist.Add(myID); // ID
                mylist.Add(partno); // partno
                mylist.Add(paramArray[i]); // batchno
                mylist.Add(paramArray[i + 1]); // qty
                mylist.Add(runningno); //running no
                mylist.Add("kode1");
                mylist.Add(kode2); //kode 2
                mylist.Add(kode2); //kode 3
                mylist.Add("expdate");

                //mylist.Add(paramArray[i]); // ID
                //mylist.Add(paramArray[i + 2]); // partno
                //mylist.Add(paramArray[i + 4]); // batchno
                //mylist.Add(paramArray[i + 5]); // qty
                //mylist.Add(paramArray[43]); //running no
                //mylist.Add("kode1");
                //mylist.Add(paramArray[i + 1]); //kode 2
                //mylist.Add(paramArray[i + 1]); //kode 3

                i = i + 3;
                li.Add(mylist);
            }
            //li.Add(mylist);

            PartReceivedDetail myPage = new PartReceivedDetail();
            int savedet=myPage.instdtReceiveDet.SaveDtUpload(li);
            if (savedet > 0)
            {
                myRes += "|" + " " + "Thank You";
                //myPage.BindForm();
            }
            else
            {
                myRes = "[FAILED]| Register PartNo First";
            }

            return myRes;
            

        }


        //[WebMethod()]
        //public static string ScanData(string param)
        //{
        //    string myre = "[SUCCESS]"; //hf_ID.Value.Trim();
        //    string[] paramArray = param.Split(',');
        //    List<List<string>> li = new List<List<string>>();
        //    List<string> mylist = new List<string>();

        //    for (int i = 0; i < (paramArray.Length - 10) / 4; i++)
        //    {
        //        myre += "|" + paramArray[i];
        //        // mylist.Add(paramArray[i] + "," + paramArray[i + 1]  + "," + paramArray[i + 2] + "," + paramArray[i + 3]);
        //        mylist.Add(myre);

        //        Mylist = new list<string>();
        //        mylist.Add(paramArray[i + 1]);
        //        mylist.Add(paramArray[i + 3]);
        //        mylist.Add(paramArray[i + 4]);


        //        i = i + 3;
        //        li.Add(mylist);
        //    }


        //    PartReceivedDetail myPage = new PartReceivedDetail();
        //    int myres = myPage.instdtReceiveDet.SaveDtUpload(li);
        //    return myres.ToString();
        //}
    }

}