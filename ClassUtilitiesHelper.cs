﻿using ClosedXML.Excel;
using SpreadsheetLight;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.OleDb;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WebBOSys.Helper
{
    public class ClassUtilitiesHelper
    {
        #region Misc



        public string Terbilang(long nilai)
        {
            string[] bilangan = { "", "satu", "dua", "tiga", "empat",
                                "lima", "enam", "tujuh", "delapan",
                                "sembilan", "sepuluh", "sebelas" };
            if (nilai < 12)
                return " " + bilangan[nilai];
            else if (nilai < 20)
                return Terbilang(nilai - 10) + " belas";
            else if (nilai < 100)
                return Terbilang((int)(nilai / 10)) + " puluh" + Terbilang(nilai % 10);
            else if (nilai < 200)
                return " seratus" + Terbilang(nilai - 100);
            else if (nilai < 1000)
                return Terbilang((int)(nilai / 100)) + " ratus" + Terbilang(nilai % 100);
            else if (nilai < 2000)
                return " seribu" + Terbilang(nilai - 1000);
            else if (nilai < 1000000)
                return Terbilang((int)(nilai / 1000)) + " ribu" + Terbilang(nilai % 1000);
            else if (nilai < 1000000000)
                return Terbilang((int)(nilai / 1000000)) + " juta" + Terbilang(nilai % 1000000);
            else if (nilai < 1000000000000)
                return Terbilang((int)(nilai / 1000000000)) + " milyar" + Terbilang(nilai % 1000000000);
            else if (nilai < 1000000000000000)
                return Terbilang((int)(nilai / 1000000000000)) + " trilyun" + Terbilang(nilai % 1000000000000);
            else
                return "";
        }
        public string safeString(string str)
        {
            if (!string.IsNullOrEmpty(str))
            {
                str = str.Replace("'", "''").Trim();
            }
            return str;
        }
        public int safeInt(string obj)
        {
            int result = 0;
            try
            {
                result = Convert.ToInt32(obj);
            }
            catch (Exception ex) { Console.WriteLine(ex.ToString()); }
            return result;
        }
        public decimal safeDecimal(string obj)
        {
            decimal result = 0.00M;
            try
            {
                result = Convert.ToDecimal(obj);
            }
            catch (Exception ex) { Console.WriteLine(ex.ToString()); }
            return result;
        }

        public void ConvertToExcel(DataTable dtTemp, string filename, Page p, string[] HeaderField)
        {
            try
            {

                GridView gv = new GridView();
                gv.DataSource = setExcelTable(dtTemp, HeaderField);
                gv.AllowPaging = false;
                gv.AllowSorting = false;
                gv.HeaderStyle.ForeColor = System.Drawing.Color.Red;
                gv.DataBind();


                string attachment = string.Empty;
                attachment = "attachment; filename=" + filename + ".xls";
                if (string.IsNullOrEmpty(filename))
                {
                    attachment = "attachment; filename=data1.xls";
                }

                p.Response.ClearContent();

                p.Response.AddHeader("content-disposition", attachment);

                p.Response.ContentType = "application/vnd.ms-excel";

                StringWriter sw = new StringWriter();

                HtmlTextWriter htw = new HtmlTextWriter(sw);

                gv.RenderControl(htw);

                p.Response.Write(sw.ToString());

                p.Response.End();

            }
            catch (Exception ex) { }

        }

        private DataTable setExcelTable(DataTable dtTemp, string[] columnField)
        {
            if (dtTemp.Rows.Count != 0)
            {
                for (int j = 0; j < columnField.Length; j++)
                {
                    for (int i = 0; i < dtTemp.Columns.Count; i++)
                    {
                        if (dtTemp.Columns[i].ColumnName.Trim() == columnField[j].ToString().Trim())
                        {
                            dtTemp.Columns[i].SetOrdinal(j);
                        }
                    }
                }
                int coun = dtTemp.Columns.Count - 1;
                for (int i = coun; i >= columnField.Length; i--)
                {
                    dtTemp.Columns.Remove(dtTemp.Columns[i]);
                }
            }
            return dtTemp;
        }
        public DataTable setFieldArangementDT(DataTable dtTemp, string[] columnField)
        {
            if (dtTemp.Rows.Count != 0)
            {
                for (int j = 0; j < columnField.Length; j++)
                {
                    for (int i = 0; i < dtTemp.Columns.Count; i++)
                    {
                        if (dtTemp.Columns[i].ColumnName.Trim() == columnField[j].ToString().Trim())
                        {
                            //dtTemp.Columns.Remove(dtTemp.Columns[i].ColumnName);
                            dtTemp.Columns[i].SetOrdinal(j);
                        }
                    }
                }
            }
            return dtTemp;
        }


        /// <summary>
        /// Membuat format Currency(###,####) untuk nilai uang 
        /// </summary>
        /// <param name="OriginalValue"></param>
        /// <param name="decimalLength"></param>
        /// <returns></returns>
        public string setCurrencyFormat(string OriginalValue, int decimalLength)
        {
            string temp = OriginalValue;
            string zeroChar = string.Empty;
            string amountNoDec = string.Empty;

            for (int i = 0; i < decimalLength; i++)
            {
                zeroChar += "0";
            }
            string decimalChar = ".";
            string Decimaldigit = string.Empty;
            if (!string.IsNullOrEmpty(OriginalValue))
            {
                string myValue = decimal.Round(Convert.ToDecimal(OriginalValue), decimalLength).ToString();
                if (myValue.Contains(decimalChar))
                {
                    amountNoDec = OriginalValue.Substring(0, OriginalValue.IndexOf(decimalChar));
                    Decimaldigit = (myValue.Substring(myValue.IndexOf(decimalChar), myValue.Length - myValue.IndexOf(decimalChar))).Replace(decimalChar, "");
                    Decimaldigit = decimalChar + (Decimaldigit + zeroChar).Substring(0, decimalLength);
                }
                else
                {
                    Decimaldigit = decimalChar + zeroChar;
                    amountNoDec = OriginalValue;
                }

                if (Convert.ToDouble(amountNoDec) != 0) amountNoDec = (Convert.ToDouble(amountNoDec)).ToString("###,###");

                //Decimaldigit = Source.Substring(Source.IndexOf("."), Source.Length - Source.IndexOf("."));
                temp = amountNoDec + Decimaldigit;

            }
            if (temp.EndsWith(".")) temp = temp.Remove(temp.Length - 1);
            return temp;
        }

        public string setCurrencyFormat(string OriginalValue)
        {
            string temp = OriginalValue;
            string zeroChar = string.Empty;
            string amountNoDec = string.Empty;
            int decimalLength = 0;

            string decimalChar = ".";
            string Decimaldigit = string.Empty;
            if (!string.IsNullOrEmpty(OriginalValue))
            {
                //string myValue = decimal.Round(Convert.ToDecimal(OriginalValue), decimalLength).ToString();
                if (OriginalValue.Contains(decimalChar))
                {
                    amountNoDec = OriginalValue.Substring(0, OriginalValue.IndexOf(decimalChar));

                    decimalLength = OriginalValue.Length - (OriginalValue.IndexOf(decimalChar) + 1);
                    for (int i = 0; i < decimalLength; i++)
                    {
                        zeroChar += "0";
                    }

                    Decimaldigit = (OriginalValue.Substring(OriginalValue.IndexOf(decimalChar), OriginalValue.Length - OriginalValue.IndexOf(decimalChar))).Replace(decimalChar, "");
                    Decimaldigit = decimalChar + (Decimaldigit + zeroChar).Substring(0, decimalLength);
                }
                else
                {
                    amountNoDec = OriginalValue;
                }

                if (Convert.ToDouble(amountNoDec) != 0) amountNoDec = (Convert.ToDouble(amountNoDec)).ToString("###,###");

                //Decimaldigit = Source.Substring(Source.IndexOf("."), Source.Length - Source.IndexOf("."));
                temp = amountNoDec + Decimaldigit;

            }
            if (temp.EndsWith(".")) temp = temp.Remove(temp.Length - 1);
            return temp;
        }

        /// <summary>
        /// Membuang Fromat Currency ( ###,###,##) menjadi nilai sebenarnya
        /// </summary>
        /// <param name="textSource">Text yang berformat currency </param>
        /// <returns></returns>
        public string removeCurrencyFormat(string textSource)
        {
            string temp = string.Empty;
            if (!string.IsNullOrEmpty(textSource))
                temp = textSource.Replace(",", "");

            return temp;
        }


        #region Create_Tree_Field


        /// <summary>
        /// Membuat Tree pada salah satu Field DataTable
        /// </summary>
        /// <param name="dt_Source">DataTable Sumber</param>
        /// <param name="Field_Id">Field Acuan pembuat Tree</param>
        /// <param name="Field_Name">Nama Filed/Kolom yang akan dijadikan bentuk Tree </param>
        /// <param name="Field_parentName">Nama Field Parent</param>
        /// <param name="MainParentCriteria">Syarat untuk menentukan Parent Utama, Contoh : "ParentIs=0"</param>
        /// <param name="levelTree">Jumlah Space Pembeda Antara Parent dan Child</param>
        /// <returns></returns>
        public DataTable DataTableToTreeField(DataTable dt_Source, string Field_Id, string Field_Name, string Field_parentName, string MainParentCriteria, int levelTree)
        {

            DataTable dtTemp = new DataTable();
            DataView dv = dt_Source.DefaultView;
            dv.Sort = Field_parentName + " asc";

            dtTemp = dv.ToTable();


            DataTable dtdd = new DataTable();

            for (int a = 0; a < dtTemp.Columns.Count; a++)
            {
                dtdd.Columns.Add(dtTemp.Columns[a].ToString());
            }


            DataRow drdd;


            int i = 0;
            int l;
            try
            {
                foreach (DataRow rowItem in dtTemp.Select(MainParentCriteria))
                {

                    l = i + 1;
                    dtdd.Rows.Add(l);
                    drdd = dtdd.Rows[i];

                    for (int j = 0; j < dtTemp.Columns.Count; j++)
                    {


                        if (dtTemp.Columns[j].ColumnName.ToString().ToLower() == Field_parentName.ToLower())
                        {
                            drdd[dtTemp.Columns[j].ToString()] = "0";

                        }
                        else
                        {
                            drdd[dtTemp.Columns[j].ToString()] = rowItem[dtTemp.Columns[j].ToString()].ToString();
                        }

                    }

                    string parentdd = safeString(rowItem[Field_Id].ToString());

                    i = dtdd.Rows.Count;

                    TreeChildAdd(parentdd, levelTree, levelTree, dtTemp, l, i, dtdd, Field_parentName, Field_Id, Field_Name);


                    i = dtdd.Rows.Count;
                }
            }
            catch
            {

            }
            finally
            {
                dtdd.Dispose();
            }

            return dtdd;
        }

        private void TreeChildAdd(string parentdd, int level, int spaceCount, DataTable Dt_Source, int l, int i, DataTable dtdd, string parentFieldName, string ParamFieldId, string paramFieldName)
        {

            DataRow drOrg;
            string parentForChild = parentdd;


            foreach (DataRow rows in Dt_Source.Select(parentFieldName + "=" + parentdd))
            {

                l = i + 1;
                i = dtdd.Rows.Count; ;

                dtdd.Rows.Add(l);
                drOrg = dtdd.Rows[i];

                for (int j = 0; j < Dt_Source.Columns.Count; j++)
                {

                    if (Dt_Source.Columns[j].ColumnName.ToString().ToLower() == paramFieldName.ToLower())
                    {
                        drOrg[Dt_Source.Columns[j].ToString()] = SpaceString(level) + rows[paramFieldName].ToString();

                    }
                    else
                    {
                        drOrg[Dt_Source.Columns[j].ToString()] = rows[Dt_Source.Columns[j].ToString()].ToString();
                    }

                }


                parentdd = safeString(rows[ParamFieldId].ToString());


                i = dtdd.Rows.Count;
                TreeChildAdd(parentdd, level + spaceCount, spaceCount, Dt_Source, l, i, dtdd, parentFieldName, ParamFieldId, paramFieldName);
            }



        }

        public void TreeBindTreeView(TreeView TreeItem, DataTable dtTemp, string TreeFieldID, string TreeFieldName, string ParentFieldIDName, string ParentFieldIDValue, int level)
        {
            DataTable dt = new DataTable();
            string parentid = string.Empty;
            TreeItem.Nodes.Clear();
            TreeNode root = new TreeNode("Menu", "0");
            TreeItem.Nodes.Add(root);
            root.Value = "Root";
            foreach (DataRow drow in dtTemp.Select(ParentFieldIDName + "=" + ParentFieldIDValue.Trim()))
            {
                TreeNode tnc = new TreeNode(drow[TreeFieldName].ToString(), drow[TreeFieldID].ToString());
                root.ChildNodes.Add(tnc);
                tnc.Value = drow[TreeFieldID].ToString();
                parentid = string.Empty;
                parentid = drow[TreeFieldID].ToString();
                TreeRecursiveMenu(dtTemp, TreeFieldID, TreeFieldName, ParentFieldIDName, parentid, level, tnc);
            }


        }
        private void TreeRecursiveMenu(DataTable dtTemp, string TreeFieldID, string TreeFieldName, string ParentFieldIDName, string ParentFieldIDValue, int level, TreeNode ChildNode)
        {

            string parentid = string.Empty;
            foreach (DataRow drow in dtTemp.Select(ParentFieldIDName + "=" + ParentFieldIDValue.Trim()))
            {
                TreeNode node = new TreeNode(drow[TreeFieldName].ToString(), drow[TreeFieldID].ToString());
                ChildNode.ChildNodes.Add(node);
                parentid = string.Empty;
                parentid = drow[TreeFieldID].ToString();
                TreeRecursiveMenu(dtTemp, TreeFieldID, TreeFieldName, ParentFieldIDName, parentid, level + 1, node);
            }
        }

        public string SpaceString(int count)
        {
            if (count == 0)
            {
                return string.Empty;
            }
            string[] s = new string[count];
            for (int i = 0; i < count; ++i)
            {
                s[i] = "&nbsp;";
            }
            return HttpUtility.HtmlDecode(string.Join("", s));
        }


        # endregion
        /// <summary>
        /// Memotong string 
        /// </summary>
        /// <param name="originString">string awal</param>
        /// <param name="cuttingString">pemotongnya</param>
        /// <param name="loop">n pemotong</param>
        /// <returns></returns>
        public string StringGetCutting(string originString, string cuttingString, int loop)
        {
            string result = string.Empty;
            string temp = string.Empty;
            int length = 0;
            int indeks = 0;
            temp = originString;
            for (int i = 0; i < loop; i++)
            {
                indeks = temp.LastIndexOf(cuttingString);
                length = temp.Length;
                if (indeks >= 1)
                {
                    result += temp.Substring(indeks + 1, length - (indeks + 1));
                    temp = temp.Remove(indeks);
                }
            }
            return result;

        }
        /// <summary>
        /// Mendapatkan substring yang digabung
        /// </summary>
        /// <param name="originString">origanal</param>
        /// <param name="cuttingString">pembatas</param>
        /// <param name="loop">n pembatas</param>
        /// <returns></returns>
        public string StringGetSub(string originString, string cuttingString, int loop)
        {
            string temp = string.Empty;
            string temp1 = string.Empty;
            int length = 0;
            int indeks = 0;
            temp = originString;
            for (int i = 0; i < loop; i++)
            {
                indeks = temp.LastIndexOf(cuttingString);
                length = temp.Length;
                if (indeks >= 1)
                {
                    temp1 = "_" + temp.Substring(indeks + 1, length - (indeks + 1)) + temp1;
                    temp = temp.Remove(indeks);
                    if (i == (loop - 1))
                    {
                        temp = temp + temp1;
                    }
                }
            }
            return temp;

        }
        public object[] StringToArray(string input, string separator, Type type)
        {
            string[] stringList = input.Split(separator.ToCharArray(),
                                              StringSplitOptions.RemoveEmptyEntries);
            object[] list = new object[stringList.Length];

            for (int i = 0; i < stringList.Length; i++)
            {
                list[i] = Convert.ChangeType(stringList[i], type);
            }

            return list;
        }
        public double DataTableGetTotalFromField(DataTable dt, string field)
        {
            double total = 0;
            foreach (DataRow dr in dt.Rows)
            {
                string data = dr[field].ToString();
                try
                {
                    total += Convert.ToDouble(data);
                }
                catch (Exception ex) { Console.WriteLine(ex.ToString()); }
                finally
                {
                    dt.Dispose();
                }
            }
            return total;
        }
        public bool DateTimeComparing(DateTime dt1, DateTime dt2)
        {
            bool result = false;
            if (DateTime.Compare(dt1, dt2) > 0) result = true;
            if (DateTime.Compare(dt1, dt2) == 0) result = true;
            if (DateTime.Compare(dt1, dt2) < 0) result = false;
            return result;
        }
        public enum ValueMethodeGetter
        {
            Empty = 0,
            KeyIn = 1,
            Option = 2,
            Date = 3,
            Attachment = 4
        }
        public bool DecimalIsValue(string data, int desimal)
        {
            bool bola = false;
            int data_length = 0;
            data_length = data.Length;
            if (data_length > 0)
            {
                int indeksDot = 0;
                indeksDot = data.IndexOf(".");
                if (indeksDot > 0)
                {
                    int check = 0;
                    check = data_length - (indeksDot + 1);
                    bool titik = false;
                    if (check <= desimal)
                    {
                        for (int i = 0; i < data_length; i++)
                        {
                            if (data[i].ToString().Contains("0") || data[i].ToString().Contains("1") || data[i].ToString().Contains("2") || data[i].ToString().Contains("3") || data[i].ToString().Contains("4") || data[i].ToString().Contains("5") || data[i].ToString().Contains("6") || data[i].ToString().Contains("7") || data[i].ToString().Contains("8") || data[i].ToString().Contains("9") || data[i].ToString().Contains("."))
                            {
                                bola = true;
                                if (data[i].ToString().Trim() == ".")
                                {
                                    if (titik == true)
                                    {
                                        bola = false;
                                        break;
                                    }
                                    else
                                    {
                                        titik = true;
                                    }
                                }
                            }
                            else
                            {
                                bola = false;
                                break;
                            }

                        }
                    }
                }
                else
                {
                    for (int i = 0; i < data_length; i++)
                    {
                        if (data[i].ToString().Contains("0") || data[i].ToString().Contains("1") || data[i].ToString().Contains("2") || data[i].ToString().Contains("3") || data[i].ToString().Contains("4") || data[i].ToString().Contains("5") || data[i].ToString().Contains("6") || data[i].ToString().Contains("7") || data[i].ToString().Contains("8") || data[i].ToString().Contains("9") || data[i].ToString().Contains("."))
                        {
                            bola = true;
                        }
                        else
                        {
                            bola = false;
                            break;
                        }

                    }
                }
            }
            return bola;
        }




        #endregion Misc
        #region SortingPaging
        private string GridViewSortDirection;
        private string GridViewSortExpression;
        public string GVSortDirection
        {
            get { return GridViewSortDirection; }
            set { GridViewSortDirection = value; }
        }
        public string GVSortExpression
        {
            get { return GridViewSortExpression ?? string.Empty; }
            set { GridViewSortExpression = value; }
        }
        private string GetSortDirection()
        {

            switch (GridViewSortDirection)
            {

                case "ASC":

                    GridViewSortDirection = "DESC";

                    break;



                case "DESC":

                    GridViewSortDirection = "ASC";

                    break;

            }

            return GridViewSortDirection;

        }
        public DataView SortDataTable(DataTable dataTable, bool isPageIndexChanging)
        {

            if (dataTable != null)
            {

                DataView dataView = new DataView(dataTable);

                if (!string.IsNullOrEmpty(GridViewSortExpression))
                {

                    if (isPageIndexChanging)
                    {
                        dataView.Sort = string.Format("{0} {1}", GridViewSortExpression, GridViewSortDirection);
                    }

                    else
                    {
                        dataView.Sort = string.Format("{0} {1}", GridViewSortExpression, GetSortDirection());
                    }
                }

                return dataView;

            }

            else
            {

                return new DataView();

            }

        }

        //private string GridViewSortDirection2
        //{
        //    get { return ViewState["SortDirection"] as string ?? "DESC"; }
        //    set { ViewState["SortDirection"] = value; }
        //}
        //private string GridViewSortExpression2
        //{
        //    get { return ViewState["SortExpression"] as string ?? string.Empty; }
        //    set { ViewState["SortExpression"] = value; }
        //}
        public void GVSorting(GridView gv, object sender, GridViewSortEventArgs e, DataTable dtTemp, Page p)
        {
            /*
              private string GridViewSortDirection
    {
        get { return ViewState["SortDirection"] as string ?? "DESC"; }
        set { ViewState["SortDirection"] = value; }
    }
    private string GridViewSortExpression
    {
        get { return ViewState["SortExpression"] as string ?? string.Empty; }
        set { ViewState["SortExpression"] = value; }
    }
    private void setSorting(GridView gv, object sender, GridViewSortEventArgs e, bool search, string tableName)
    {
        DataTable dtTemp = new DataTable();
        dtTemp = this.GetLabelList(search, tableName);
        GridViewSortExpression = e.SortExpression;
        this.pgc.GVSortExpression = GridViewSortExpression;
        this.pgc.GVSortDirection = GridViewSortDirection;
        int pageIndex = gv.PageIndex;
        gv.DataSource = this.pgc.SortDataTable(dtTemp, false);
        GridViewSortDirection = this.pgc.GVSortDirection;
        gv.DataBind();
        gv.PageIndex = pageIndex;
    }
    private void setPaging(GridView gv, object sender, GridViewPageEventArgs e, bool search, string tableName)
    {
        DataTable dtTemp = new DataTable();
        dtTemp = this.GetLabelList(search, tableName);
        gv.SelectedIndex = -1;
        this.pgc.GVSortExpression = GridViewSortExpression;
        this.pgc.GVSortDirection = GridViewSortDirection;
        gv.DataSource = this.pgc.SortDataTable(dtTemp, true);
        gv.PageIndex = e.NewPageIndex;
        gv.DataBind();
    }
             */

            if (HttpContext.Current.Session["SortDirection"] == null)
            {
                HttpContext.Current.Session["SortDirection"] = "DESC";
            }
            GridViewSortExpression = e.SortExpression;
            this.GVSortExpression = GridViewSortExpression;
            this.GVSortDirection = HttpContext.Current.Session["SortDirection"].ToString();
            int pageIndex = gv.PageIndex;
            gv.DataSource = this.SortDataTable(dtTemp, false);
            GridViewSortDirection = this.GVSortDirection;
            HttpContext.Current.Session["SortDirection"] = GridViewSortDirection;
            gv.DataBind();
            gv.PageIndex = pageIndex;
        }
        public void GVPaging(GridView gv, string indeks, DataTable dtTemp)
        {
            int indek = 0;
            int data1 = 0;
            int data2 = 0;
            indek = this.safeInt(indeks);
            if (indek <= 0)
                indek = 1;
            indek = indek - 1;
            data1 = dtTemp.Rows.Count;
            if (data1 > 0)
            {
                data2 = gv.PageCount;
                if (indek < 0 || indek > data2)
                    indek = data2;
                gv.DataSource = this.SortDataTable(dtTemp, true);
                gv.PageIndex = indek;
                gv.DataBind();
            }
        }
        #endregion
        public List<string> loadCsvFile(string filePath)
        {
            var reader = new StreamReader(File.OpenRead(filePath));
            List<string> searchList = new List<string>();
            while (!reader.EndOfStream)
            {
                var line = reader.ReadLine();
                searchList.Add(line);
            }
            return searchList;
        }

        public DataTable CsvToDataTable(string path, bool isFirstRowHeader)
        {
            DataTable dataTable = new DataTable();
            string header = isFirstRowHeader ? "Yes" : "No";

            string pathOnly = Path.GetDirectoryName(path);
            string fileName = Path.GetFileName(path);

            string sql = @"SELECT * FROM [" + fileName + "]";

            using (OleDbConnection connection = new OleDbConnection(
                      @"Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" + pathOnly +
                      ";Extended Properties=\"Text;HDR=" + header + "\""))
            using (OleDbCommand command = new OleDbCommand(sql, connection))
            using (OleDbDataAdapter adapter = new OleDbDataAdapter(command))
            {
                
                dataTable.Locale = CultureInfo.CurrentCulture;
                adapter.Fill(dataTable);
                connection.Close();
               
            }
            return dataTable;
        }
        public DataTable ExcelToDataTableOr( string path, string sheetName)
        {
            if (string.IsNullOrEmpty(sheetName)) sheetName = "sheet1";
            using (OleDbConnection conn = new OleDbConnection())
            {
                DataTable dt = new DataTable();
                string Import_FileName = path;
                string fileExtension = Path.GetExtension(Import_FileName);
                if (fileExtension == ".xls")
                    conn.ConnectionString = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" + Import_FileName + ";" + "Extended Properties='Excel 8.0;HDR=YES;'";
                if (fileExtension == ".xlsx")
                    conn.ConnectionString = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + Import_FileName + ";" + "Extended Properties='Excel 12.0 Xml;HDR=YES;'";

               
                using (OleDbCommand comm = new OleDbCommand())
                {
                    sheetName = sheetName.Replace("$", "");
                    comm.CommandText = "Select * from [" + sheetName + "$]";
                    comm.Connection = conn;
                    using (OleDbDataAdapter da = new OleDbDataAdapter())
                    {
                        da.SelectCommand = comm;
                        da.Fill(dt);
                        return dt;
                    }
                }
            }
        }
        public System.Data.DataTable ExcelToDataTable(string path,  string sheetName)
        {
            string[] columnDatetimeList=new string[] { };
            System.Data.DataTable dtExcel = new System.Data.DataTable();
            dtExcel.TableName = "dtExcel";
            //pesan = "kosong";
            try
            {
                if (File.Exists(path))
                {
                   // pesan = "Ada";
                    using (SLDocument sl = new SLDocument(path))
                    {
                        SLWorksheetStatistics stats = sl.GetWorksheetStatistics();
                        int startRowIndex = stats.StartRowIndex;
                        int endRowIndex = stats.EndRowIndex;
                        dtExcel = ExcelCreateColumnInfo(sl);
                        System.Data.DataTable dtExcel2 = new System.Data.DataTable();
                        bool isDatetime = false;
                        string columnResult = string.Empty;
                        string columnName = string.Empty;
                        DataRow dr;
                        DateTime time;
                        for (int i = startRowIndex + 1; i <= endRowIndex; i++) //Start dari i+1 =2, karena 1 adadalh column
                        {
                            dr = dtExcel.NewRow();
                            foreach (DataColumn item in dtExcel.Columns)
                            {

                                int columnIndex = Convert.ToInt16(item.Caption.ToString());
                                columnName = item.ColumnName.Trim();
                                columnResult = sl.GetCellValueAsString(i, columnIndex).Trim();
                                isDatetime = ExcelIsDatetimeColumn(columnName, columnDatetimeList);
                                if (isDatetime)
                                {

                                    time = new DateTime();
                                    time = sl.GetCellValueAsDateTime(i, columnIndex, "");
                                    columnResult = time.ToString("yyyy-MM-dd HH:mm:ss");
                                }
                                dr[columnName] = columnResult;
                            }
                            dtExcel.Rows.Add(dr);
                        }
                    }
                }
                else
                {
                    //pesan = "tidak ada";
                }
            }
            catch (Exception ex)
            {

               // pesan = "esceprsionsd error_";
                throw ex;
            }
            finally
            {
                dtExcel.Dispose();
            }
            return dtExcel;
        }


        public DataTable ExcelGetSheetListOr(string filePath)
        {
            DataTable dtexcel = new DataTable();
            bool hasHeaders = false;
            string HDR = hasHeaders ? "Yes" : "No";
            string strConn;
            if (filePath.Substring(filePath.LastIndexOf('.')).ToLower() == ".xlsx")
                strConn = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + filePath + ";Extended Properties=\"Excel 12.0;HDR=" + HDR + ";IMEX=0\"";
            else
                strConn = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" + filePath + ";Extended Properties=\"Excel 8.0;HDR=" + HDR + ";IMEX=0\"";
            OleDbConnection conn = new OleDbConnection(strConn);
            conn.Open();
            DataTable schemaTable = conn.GetOleDbSchemaTable(OleDbSchemaGuid.Tables, new object[] { null, null, null, "TABLE" });
            for (int i = 0; i < schemaTable.Rows.Count; i++)
            {
                schemaTable.Rows[i]["Table_Name"] = schemaTable.Rows[i]["Table_Name"].ToString().Replace("'", "").Replace("$","");
            }
            DataView dv = new DataView(schemaTable);
            dv.RowFilter = "Table_Name not like '%FilterDatabase%'";
            schemaTable = dv.ToTable();
            conn.Close();
            //Looping Total Sheet of Xl File
            /*foreach (DataRow schemaRow in schemaTable.Rows)
            {
            }*/
            //Looping a first Sheet of Xl File
             return schemaTable;

        }
        public DataTable ExcelGetSheetList(string filePath)
        {
            DataTable dt = new DataTable();
            dt.TableName = "Sheets";
            dt.Columns.Add("No");
            dt.Columns.Add("Table_Name");
            int i = 1;
            using (SLDocument sl = new SLDocument(filePath))
            {
                foreach (var sheetName in sl.GetWorksheetNames())
                {

                    DataRow dr = dt.NewRow();
                    dr["Table_Name"] = sheetName.ToString();
                    dr["No"] = i.ToString();
                    dt.Rows.Add(dr);
                    i++;
                }
            }
            return dt;
        }

        public DataTable ExcelToDataTable(string filePath)
        {
            DataTable dtexcel = new DataTable();
            bool hasHeaders = false;
            string HDR = hasHeaders ? "Yes" : "No";
            string strConn;
            if (filePath.Substring(filePath.LastIndexOf('.')).ToLower() == ".xlsx")
                strConn = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + filePath + ";Extended Properties=\"Excel 12.0;HDR=" + HDR + ";IMEX=0\"";
            else
                strConn = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" + filePath + ";Extended Properties=\"Excel 8.0;HDR=" + HDR + ";IMEX=0\"";
            OleDbConnection conn = new OleDbConnection(strConn);
            conn.Open();
            DataTable schemaTable = conn.GetOleDbSchemaTable(OleDbSchemaGuid.Tables, new object[] { null, null, null, "TABLE" });
            //Looping Total Sheet of Xl File
            /*foreach (DataRow schemaRow in schemaTable.Rows)
            {
            }*/
            //Looping a first Sheet of Xl File
            DataRow schemaRow = schemaTable.Rows[0];
            string sheet = schemaRow["TABLE_NAME"].ToString();
            if (!sheet.EndsWith("_"))
            {
                string query = "SELECT  * FROM [" + sheet + "]";
                OleDbDataAdapter daexcel = new OleDbDataAdapter(query, conn);
                dtexcel.Locale = CultureInfo.CurrentCulture;
                daexcel.Fill(dtexcel);
            }

            conn.Close();
            return dtexcel;

        }
        /*-----------------------------------------------------------------------------------------------------*/
        /// <summary>
        /// ConvertExcelToDatatableOpenXML
        /// </summary>
        /// <param name="pesan"></param>
        /// <param name="filename"></param>
        /// <param name="columnDatetimeList"></param>
        /// <returns></returns>
        public System.Data.DataTable ExcelToDatatableOpenXML(out string pesan, string filename, string[] columnDatetimeList)
        {
            System.Data.DataTable dtExcel = new System.Data.DataTable();
            dtExcel.TableName = "dtExcel";
            pesan = "kosong";
            try
            {
                if (File.Exists(filename))
                {
                    pesan = "Ada";
                    using (SLDocument sl = new SLDocument(filename))
                    {
                        SLWorksheetStatistics stats = sl.GetWorksheetStatistics();
                        int startRowIndex = stats.StartRowIndex;
                        int endRowIndex = stats.EndRowIndex;
                        dtExcel = ExcelCreateColumnInfo(sl);
                        System.Data.DataTable dtExcel2 = new System.Data.DataTable();
                        bool isDatetime = false;
                        string columnResult = string.Empty;
                        string columnName = string.Empty;
                        DataRow dr;
                        DateTime time;
                        for (int i = startRowIndex + 1; i <= endRowIndex; i++) //Start dari i+1 =2, karena 1 adadalh column
                        {
                            dr = dtExcel.NewRow();
                            foreach (DataColumn item in dtExcel.Columns)
                            {

                                int columnIndex = Convert.ToInt16(item.Caption.ToString());
                                columnName = item.ColumnName.Trim();
                                columnResult = sl.GetCellValueAsString(i, columnIndex).Trim();
                                isDatetime = ExcelIsDatetimeColumn(columnName, columnDatetimeList);
                                if (isDatetime)
                                {

                                    time = new DateTime();
                                    time = sl.GetCellValueAsDateTime(i, columnIndex, "");
                                    columnResult = time.ToString("yyyy-MM-dd HH:mm:ss");
                                }
                                dr[columnName] = columnResult;
                            }
                            dtExcel.Rows.Add(dr);
                        }
                    }
                }
                else
                {
                    pesan = "tidak ada";
                }
            }
            catch (Exception ex)
            {

                pesan = "esceprsionsd error_";
                throw ex;
            }
            finally
            {
                dtExcel.Dispose();
            }
            return dtExcel;
        }
      

        private System.Data.DataTable ExcelCreateColumnInfo(SLDocument sl)
        {

            System.Data.DataTable dtExcel = new System.Data.DataTable();
            SLWorksheetStatistics stats = sl.GetWorksheetStatistics();
            int startColumn = stats.StartColumnIndex;
            int endColumn = stats.EndColumnIndex;

            string columnName = string.Empty;
            DataColumn dc;
            for (int i = startColumn; i <= endColumn; i++)
            {
                columnName = sl.GetCellValueAsString(1, i).Trim();
                if (!string.IsNullOrEmpty(columnName))
                {
                    dc = new DataColumn();
                    foreach (DataColumn dcs in dtExcel.Columns)
                    {
                        if (columnName == dc.ColumnName.Trim())
                        {
                            columnName = columnName + "_1";
                            break;
                        }

                    }
                    dc.ColumnName = columnName;
                    dc.Caption = i.ToString();
                    dtExcel.Columns.Add(dc);

                }
                else
                {
                    break;
                }

            }

            return dtExcel;

        }
        private bool ExcelIsDatetimeColumn(string columnName, string[] columnDatetimeList)
        {
            bool bola = false;
            foreach (var item in columnDatetimeList)
            {
                if (columnName.Trim().ToUpper() == item.Trim().ToUpper())
                {
                    bola = true;
                    break;
                }
            }
            return bola;
        }

        public System.Data.DataTable ExcelGetSheetListOpenXml(string filePath)
        {
            DataTable dt = new DataTable();
            dt.TableName = "Sheets";
            dt.Columns.Add("No");
            dt.Columns.Add("Table_Name");
            int i = 1;
            using (SLDocument sl = new SLDocument(filePath))
            {
                foreach (var sheetName in sl.GetWorksheetNames())
                {
                    
                    DataRow dr = dt.NewRow();
                    dr["Table_Name"] = sheetName.ToString();
                    dr["No"] = i.ToString();
                    dt.Rows.Add(dr);
                    i++;
                }
            }
            return dt;
        }

        /*-----------------------------------------------------------------------------------------------------*/
        public DataTable ReadCSVToDataTable(string CSVUrl,char delimeter)
        {
            DataTable dt = new DataTable();
            var lines = File.ReadAllLines("e:\\mtmenu.csv").Select(a => a.Split(delimeter));
            var csv = from line in lines
                      select (from piece in line
                              select piece);
             
            //var csv2 = from line in lines
            //          select (line.Split(',')).ToArray();
            

            return dt;
        }
        public bool DataTableToTextFile(DataTable dt, string filePath, string separator, bool IsHeader)
        {
            bool result = false;
            int i = 0;
            StreamWriter sw = null;
            try
            {

                sw = new StreamWriter(filePath, false);

                if (IsHeader)
                {
                    for (i = 0; i < dt.Columns.Count - 1; i++)
                    {

                        sw.Write(dt.Columns[i].ColumnName + separator);

                    }
                    sw.Write(dt.Columns[i].ColumnName);
                    sw.WriteLine();
                }

                foreach (DataRow row in dt.Rows)
                {
                    object[] array = row.ItemArray;

                    for (i = 0; i < array.Length - 1; i++)
                    {
                        sw.Write(array[i].ToString().Trim() + separator);
                    }
                    sw.Write(array[i].ToString().Trim());
                    sw.WriteLine();

                }
                result = true;
                sw.Close();
            }

            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
            return result;
        }
        public bool DataTableToTextFile(out string error, DataTable dt, string filePath, string separator, bool IsHeader)
        {
            bool result = false; string erro = string.Empty;
            int i = 0;
            StreamWriter sw = null;
            try
            {

                sw = new StreamWriter(filePath, false);

                if (IsHeader)
                {
                    for (i = 0; i < dt.Columns.Count - 1; i++)
                    {

                        sw.Write(dt.Columns[i].ColumnName + separator);

                    }
                    sw.Write(dt.Columns[i].ColumnName);
                    sw.WriteLine();
                }

                foreach (DataRow row in dt.Rows)
                {
                    object[] array = row.ItemArray;

                    for (i = 0; i < array.Length - 1; i++)
                    {
                        sw.Write(array[i].ToString().Trim() + separator);
                    }
                    sw.Write(array[i].ToString().Trim());
                    sw.WriteLine();

                }
                result = true;
                sw.Close();
            }

            catch (Exception ex)
            {
                erro = ex.ToString();
                Console.WriteLine(ex.ToString());
            }
            error= erro;
            return result;
        }
        public bool DataTableToXls(DataTable dt,HttpResponse Response, string XlsFileName)
        {
            try{
                using (XLWorkbook wb = new XLWorkbook())
                {
                    wb.Worksheets.Add(dt);
                    wb.Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Center;
                    wb.Style.Font.Bold = true;

                    Response.Clear();
                    Response.Buffer = true;
                    Response.Charset = "";
                    Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                    Response.AddHeader("content-disposition", "attachment;filename="+XlsFileName+".xlsx");

                    using (MemoryStream MyMemoryStream = new MemoryStream())
                    {
                        wb.SaveAs(MyMemoryStream);
                        MyMemoryStream.WriteTo(Response.OutputStream);

                        Response.Flush();
                        Response.End();
                    }
                }
                return true;
            }
            catch (Exception ex) { return false; }
            
        }
        public bool checkLoginStatus(string logInID)
        {
            bool res = false;

            return res;
        }

    }
    public static class MessageBox
    {
        public static void Show(this Page Page, String Message)
        {
            Page.ClientScript.RegisterStartupScript(
               Page.GetType(),
               "MessageBox",
               "<script language='javascript'>alert('" + Message + "');</script>"
            );
        }
    }

 
}