﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="PartRequestIssuedDetail.aspx.cs" Inherits="Delogi.Form.UI.Warehouse.Transaction.PartRequestIssuedDetail" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Src="~/UI/UserControl/GeneralTitleHeader.ascx" TagPrefix="uc1" TagName="GeneralTitleHeader" %>
<%@ Register Src="~/UI/UserControl/UC_GeneralSearching.ascx" TagPrefix="uc1" TagName="UC_GeneralSearching" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link href="../../../Style/Css/GeneralStyle.css" rel="stylesheet" />
    <link href="../../../Style/Additional/bootstrap.css" rel="stylesheet" />
     <script src="../../../Scripts/html5-qrcode.min.js"></script>
    <script src="../../../Scripts/jquery-1.8.2.js" type="text/javascript"></script>
    <style type="text/css">
        .auto-style1 {
            height: 12px;
        }
    </style>
</head>
<body class="genBody" oncontextmenu="return false;">
    <form id="form1" runat="server" enctype="multipart/form-data">
        <div>
            <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
            <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                <ContentTemplate>
                    <uc1:GeneralTitleHeader runat="server" ID="GeneralTitleHeader" />
                    <div class="row">
                        <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12  ">
                            <div class="float-right">
                                <asp:Label ID="lblRefNo" runat="server" CssClass="general"></asp:Label>
                                <asp:Button ID="btnClose" runat="server" CssClass="btnExitIcon" OnClick="btnClose_Click" Style="margin-right: 5px" />
                            </div>
                        </div>
                    </div>

                    <asp:Panel ID="pnlHeader" runat="server" GroupingText="Detail" CssClass="genTable">
                        <cc1:CollapsiblePanelExtender ID="CollapsiblePanelExtender2" CollapseControlID="pnlHeaderTransInfo"
                            CollapsedImage="~/Style/Image/Main/Misc/expand.jpg" ExpandControlID="pnlHeaderTransInfo"
                            ExpandedImage="~/Style/Image/Main/Misc/collapse.jpg" TargetControlID="pnlTransInfo"
                            ImageControlID="ImageButton1" runat="server"></cc1:CollapsiblePanelExtender>
                        <asp:Panel ID="pnlHeaderTransInfo" runat="server">
                            <div>
                                <asp:ImageButton ID="ImageButton2" runat="server" ImageUrl="~/Style/Image/Main/Misc/expand.jpg" />
                                <asp:Label ID="Label29" runat="server" Text="Header Transaction Information" Font-Bold="true" CssClass="general"></asp:Label>
                            </div>
                        </asp:Panel>
                        <asp:Panel ID="pnlTransInfo" runat="server" GroupingText="-" CssClass="genTable">
                            <div class="row">
                                <div class="col-xl-5 col-lg-5 col-md-5 col-sm-5 col-11">
                                    <div class="form-group row">
                                        <asp:Label ID="Label26" runat="server" CssClass="general col-xl-3 col-lg-3 col-md-3 col-sm-4 col-3" Text="Request No"></asp:Label>
                                        <asp:Label ID="Label2" runat="server" CssClass="general col-xl-1 col-lg-1 col-md-1 col-sm-1 col-1" Text=":"></asp:Label>

                                        <asp:Label ID="lbl_ReqID" runat="server" CssClass="general label_isidata"></asp:Label>


                                    </div>

                                    <div class="form-group row">
                                        <asp:Label ID="Label3" runat="server" CssClass="general col-xl-3 col-lg-3 col-md-3 col-sm-4 col-3" Text="Request By "></asp:Label>
                                        <asp:Label ID="Label5" runat="server" CssClass="general col-xl-1 col-lg-1 col-md-1 col-sm-1 col-1" Text=":"></asp:Label>
                                        <asp:Label ID="lblRequestBy" runat="server" CssClass="general  label_isidata"></asp:Label>
                                        <asp:Label ID="lblinsert_by" runat="server" Visible="false" CssClass="general"></asp:Label>
                                    </div>

                                    <div class="form-group row">
                                        <asp:Label ID="Label1" runat="server" CssClass="general col-xl-3 col-lg-3 col-md-3 col-sm-4 col-3" Text="Request Date"></asp:Label>
                                        <asp:Label ID="Label4" runat="server" CssClass="general col-xl-1 col-lg-1 col-md-1 col-sm-1 col-1" Text=":"></asp:Label>

                                        <asp:Label ID="lblRequestDate" runat="server" CssClass="general label_isidata"></asp:Label>


                                    </div>

                                    <div class="form-group row">
                                        <asp:Label ID="Label10" runat="server" CssClass="general col-xl-3 col-lg-3 col-md-3 col-sm-4 col-3" Text="Reason"></asp:Label>
                                        <asp:Label ID="Label11" runat="server" CssClass="general col-xl-1 col-lg-1 col-md-1 col-sm-1 col-1" Text=":"></asp:Label>

                                        <asp:TextBox ID="tbReason" runat="server" Font-Names="Tahoma"
                                            Font-Size="8pt" Height="34px" TextMode="MultiLine" Width="50%"></asp:TextBox>

                                    </div>

                                </div>
                                <div class="offset-xl-1 offset-lg-1 offset-md-1"></div>
                                <div class="col-xl-5 col-lg-5 col-md-5 col-sm-5 col-11">
                                    <div class="form-group row">
                                        <asp:Label ID="Label6" runat="server" CssClass="general col-xl-3 col-lg-3 col-md-3 col-sm-4 col-3" Text="Warehouse"></asp:Label>
                                        <asp:Label ID="Label7" runat="server" CssClass="general col-xl-1 col-lg-1 col-md-1 col-sm-1 col-1" Text=":"></asp:Label>
                                        <asp:DropDownList ID="ddwhouse" runat="server" AutoPostBack="True"
                                            CssClass="general" OnSelectedIndexChanged="ddWHouseTo_SelectedIndexChanged">
                                        </asp:DropDownList>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="ddwhouse"
                                            ErrorMessage="* Insert Request to" ValidationGroup="SaveValidation"></asp:RequiredFieldValidator>

                                    </div>
                                    <div class="form-group row">
                                        <asp:Label ID="Label8" runat="server" CssClass="general col-xl-3 col-lg-3 col-md-3 col-sm-4 col-3" Text="Project"></asp:Label>
                                        <asp:Label ID="Label9" runat="server" CssClass="general col-xl-1 col-lg-1 col-md-1 col-sm-1 col-1" Text=":"></asp:Label>
                                        <asp:DropDownList ID="ddProject" runat="server" CssClass="general">
                                        </asp:DropDownList>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ValidationGroup="SaveValidation"
                                                ErrorMessage="* Insert Project" ControlToValidate="ddProject"></asp:RequiredFieldValidator>

                                    </div>
                                    <div class="form-group row">
                                        <asp:Label ID="Label12" runat="server" CssClass="general col-xl-3 col-lg-3 col-md-3 col-sm-4 col-3" Text="Next Approval"></asp:Label>
                                        <asp:Label ID="Label13" runat="server" CssClass="general col-xl-1 col-lg-1 col-md-1 col-sm-1 col-1" Text=":"></asp:Label>

                                        <asp:DropDownList ID="ddSuperior" runat="server" CssClass="general" AutoComplete="off"></asp:DropDownList>
                                        <asp:Label ID="lblSuperior" runat="server" CssClass="general" Visible="false"></asp:Label>

                                    </div>

                                       <div class="form-group row">
                                        <asp:Label ID="Label14" runat="server" CssClass="general col-xl-3 col-lg-3 col-md-3 col-sm-4 col-3" Text=""></asp:Label>
                                        <asp:Label ID="Label15" runat="server" CssClass="general col-xl-1 col-lg-1 col-md-1 col-sm-1 col-1" Text=""></asp:Label>

                                                                            <asp:Button ID="btnSave" runat="server" CssClass="btnBlue" Width="80px"
                                        Height="20px" Text="Save" Visible="false" ValidationGroup="SaveValidation"
                                        OnClick="btnSave_Click" />
                                    <asp:Button ID="Btnupdate" runat="server" CssClass="btnBlue" Height="20px" OnClick="Btnupdate_Click" Text="Update" Visible="false" Width="60px" />
                                    <asp:Button ID="btnCancel" runat="server" CssClass="btnBlue" Height="20px" OnClick="btnCancel_Click1" Text="Cancel" Visible="false" Width="60px" />
                                    <asp:Button ID="btnRequest" runat="server" Text="Request" CssClass="btnBlue" Height="20px" OnClick="btnRequest_Click" Visible="false" />


                                    </div>


                                </div>
                            </div>
                        </asp:Panel>


                        <br />
                    </asp:Panel>

                     <%--------------------------------------------------------------------------------%>
                     <asp:Panel ID="panelQRHandle" runat="server" Visible="false">
                        <table width="100%" class="genTable">
                            <tr>
                                <td width="13%">
                                   
                                </td>
                                <td width="1%">&nbsp;</td>

                                    <td width="13%" class="auto-style2">
                                               <asp:TextBox ID="tbQR" runat="server" AutoPostBack="false" Width="50%"  BorderColor="#ff0066" Enabled="true" Text="0" Visible="false"></asp:TextBox>
                                     <asp:Label ID="lblName" runat="server" CssClass="general" Text="" Visible="false"></asp:Label>
                                        <asp:Button ID="ScanQR" runat="server" CssClass="btnBlue" OnClick="ScanQR_Click" Text="Scan QR by Camera" Visible="false" />
                                   <asp:Label ID="lblEnterCount" runat="server" CssClass="general" Text="" Visible="false"></asp:Label>
                                        <br />
                                    <div id="qr-reader" style="width: 500px">
                                    </div>
                                    &nbsp;&nbsp;&nbsp;&nbsp;</td>
                                <td width="1%" class="auto-style2"></td>
                                <td width="27%">&nbsp;</td>

                            

                            </tr>
                        </table>



                    </asp:Panel>

                    <%--------------------------------------------------------------------------------%>
                    <asp:Panel ID="panelAdmin" runat="server" CssClass="genTable">

                        <asp:GridView ID="GVAdmin" runat="server" CssClass="mGrid" AutoGenerateColumns="false"
                            OnRowDataBound="GVAdmin_RowDataBound" OnRowCommand="GVAdmin_RowCommand" PageSize="30"
                            EmptyDataText="No Record" ShowFooter="true" ShowHeaderWhenEmpty="true"
                            OnSelectedIndexChanged="GVAdmin_SelectedIndexChanged">
                            <Columns>
                                <asp:TemplateField HeaderText="NO">
                                    <ItemTemplate>
                                        <asp:Label ID="lblNo" runat="server" CssClass="general">
                                        </asp:Label>
                                    </ItemTemplate>
                                    <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" Width="4%" />
                                    <HeaderStyle HorizontalAlign="Center" />
                                </asp:TemplateField>

                                <asp:TemplateField HeaderText="ReqLabID" Visible="false">
                                    <ItemTemplate>
                                        <asp:Label ID="LblReqLabID" runat="server" CssClass="general" Text='<%# Bind("ReqLabID")%>'> 
                                        </asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>

                                <asp:TemplateField HeaderText="Issued ID" Visible="false">
                                    <ItemTemplate>
                                        <asp:Label ID="lblID" runat="server" CssClass="general" Text='<%# Bind("ReqID")%>'> 
                                        </asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>

                                <asp:TemplateField HeaderText="LABEL ID" SortExpression="labelID">
                                    <ItemTemplate>
                                        <asp:Label ID="LbllabelID" runat="server" CssClass="general" Text='<%# Bind("labelID") %>'> 
                                        </asp:Label>
                                    </ItemTemplate>
                                    <FooterTemplate>
                                        <asp:TextBox ID="tbLabelID" runat="server" CssClass="general" Width="120px" AutoPostBack="true" OnTextChanged="tbLabelID_TextChanged"></asp:TextBox>
                                    </FooterTemplate>
                                    <HeaderStyle HorizontalAlign="Center" />
                                </asp:TemplateField>

                                <asp:TemplateField HeaderText="Item ID" SortExpression="itemsID" Visible="false">
                                    <ItemTemplate>
                                        <asp:Label ID="LblitemsID" runat="server" CssClass="general" Text='<%# Bind("itemsID") %>'> 
                                        </asp:Label>
                                    </ItemTemplate>
                                    <FooterTemplate>
                                        <asp:Label ID="lblitemsID" runat="server" CssClass="general"></asp:Label>
                                    </FooterTemplate>
                                    <HeaderStyle HorizontalAlign="Center" />
                                </asp:TemplateField>

                                <asp:TemplateField HeaderText="SAP PART NO" SortExpression="partCode">
                                    <ItemTemplate>
                                        <asp:Label ID="LblpartCode" runat="server" CssClass="general" Text='<%# Bind("partCode") %>'> 
                                        </asp:Label>
                                    </ItemTemplate>
                                    <FooterTemplate>
                                        <asp:Label ID="lblitemsCode" runat="server" CssClass="general"> 
                                        </asp:Label>
                                    </FooterTemplate>
                                    <HeaderStyle HorizontalAlign="Center" />
                                </asp:TemplateField>

                                <asp:TemplateField HeaderText="PART NAME" SortExpression="partName">
                                    <ItemTemplate>
                                        <asp:Label ID="LblpartName" runat="server" CssClass="general" Text='<%# Bind("partName") %>'> 
                                        </asp:Label>
                                    </ItemTemplate>
                                    <FooterTemplate>
                                        <asp:Label ID="lblitemsName" runat="server" CssClass="general"> 
                                        </asp:Label>
                                    </FooterTemplate>
                                    <HeaderStyle HorizontalAlign="Center" />
                                </asp:TemplateField>

                                <asp:TemplateField HeaderText="MATERIAL" SortExpression="partNameDesc">
                                    <ItemTemplate>
                                        <asp:Label ID="LblpartDesc" runat="server" CssClass="general" Text='<%# Bind("partNameDesc") %>'> 
                                        </asp:Label>
                                    </ItemTemplate>
                                    <FooterTemplate>
                                        <asp:Label ID="lblitemsDesc" runat="server" CssClass="general"> 
                                        </asp:Label>
                                    </FooterTemplate>
                                    <HeaderStyle HorizontalAlign="Center" />
                                </asp:TemplateField>

                                <asp:TemplateField HeaderText="Label Type" SortExpression="labelType" Visible="false">
                                    <ItemTemplate>
                                        <asp:Label ID="LbllabelType" runat="server" CssClass="general" Text='<%# Bind("labelType") %>'> 
                                        </asp:Label>
                                    </ItemTemplate>
                                    <FooterTemplate>
                                        <asp:Label ID="LbllabelType" runat="server" CssClass="general"> 
                                        </asp:Label>
                                    </FooterTemplate>
                                    <HeaderStyle HorizontalAlign="Center" />
                                </asp:TemplateField>

                                <asp:TemplateField HeaderText="ISSUED QTY" SortExpression="IssuedQty">
                                    <ItemTemplate>
                                        <asp:Label ID="lblQty" runat="server" CssClass="general" Text='<%# Bind("IssuedQty") %>'> 
                                        </asp:Label>
                                    </ItemTemplate>
                                    <FooterTemplate>
                                        <asp:Label ID="lblQty" runat="server" CssClass="general"> 
                                        </asp:Label>
                                        <asp:TextBox ID="tbQty" runat="server" Visible="false"></asp:TextBox>
                                    </FooterTemplate>
                                    <HeaderStyle HorizontalAlign="Center" />
                                </asp:TemplateField>

                                <asp:TemplateField HeaderText="BATCH NO" SortExpression="lotNo" Visible="false">
                                    <ItemTemplate>
                                        <asp:Label ID="LbllotNo" runat="server" CssClass="general" Text='<%# Bind("lotNo") %>'> 
                                        </asp:Label>
                                    </ItemTemplate>
                                    <FooterTemplate>
                                        <asp:Label ID="lblLotNo" runat="server" CssClass="general"> 
                                        </asp:Label>
                                    </FooterTemplate>
                                    <HeaderStyle HorizontalAlign="Center" />
                                </asp:TemplateField>


                                <asp:TemplateField HeaderText="BATCH NO" SortExpression="BlockName">
                                    <ItemTemplate>
                                        <asp:Label ID="lblblockname" runat="server" CssClass="general" Text='<%# Bind("blockName") %>'> 
                                        </asp:Label>
                                    </ItemTemplate>
                                    <FooterTemplate>
                                        <asp:Label ID="lblblockname" runat="server" CssClass="general"> 
                                        </asp:Label>
                                        <asp:TextBox ID="tbblockName" runat="server" Visible="false"></asp:TextBox>
                                    </FooterTemplate>
                                    <HeaderStyle HorizontalAlign="Center" />
                                </asp:TemplateField>

                                <asp:TemplateField HeaderText="QTY/BATCH" SortExpression="weightQty">
                                    <ItemTemplate>
                                        <asp:Label ID="lblweightQty" runat="server" CssClass="general" Text='<%# Bind("weightQty") %>'> 
                                        </asp:Label>
                                    </ItemTemplate>
                                    <FooterTemplate>
                                        <asp:TextBox ID="tbWeightQty" runat="server" ></asp:TextBox>
                                        <asp:Label ID="lbWeightQty" runat="server" Text="*Must be fill" ForeColor="Red" Visible="false"></asp:Label>
                                         <asp:Label ID="lbltotalScrap" runat="server" CssClass="general" ForeColor="#000099"></asp:Label>
                                    </FooterTemplate>
                                    <HeaderStyle HorizontalAlign="Center" />
                                </asp:TemplateField>


                                <asp:TemplateField HeaderText="REMARK" SortExpression="returnMark">
                                    <ItemTemplate>
                                        <asp:Label ID="LblreturnMark" runat="server" CssClass="general" Text='<%# Bind("returnMark") %>'> 
                                        </asp:Label>
                                    </ItemTemplate>
                                    <HeaderStyle HorizontalAlign="Center" />
                                </asp:TemplateField>


                                <asp:TemplateField HeaderText="RETURN QTY" SortExpression="returnQty">
                                    <ItemTemplate>
                                        <asp:Label ID="LblreturnQty" runat="server" CssClass="general" Text='<%# Bind("returnQty") %>'> 
                                        </asp:Label>
                                    </ItemTemplate>
                                    <HeaderStyle HorizontalAlign="Center" />
                                </asp:TemplateField>


                                <asp:TemplateField HeaderText="DELETE">
                                    <ItemTemplate>
                                        <asp:Button ID="btndelete" runat="server" CausesValidation="False" CommandName="del"
                                            CssClass="btnDeleteIcon" ToolTip="Delete" />
                                    </ItemTemplate>
                                    <FooterTemplate>
                                        <asp:Button ID="btnsave_RatShare" runat="server" CausesValidation="false" CommandName="save"
                                            CssClass="btnBlue" Text="Save" ToolTip="Save" />
                                    </FooterTemplate>
                                    <HeaderStyle HorizontalAlign="Center" />
                                    <FooterStyle HorizontalAlign="Center" />
                                    <ItemStyle HorizontalAlign="Center" VerticalAlign="Top" Width="7%" />
                                </asp:TemplateField>
                            </Columns>
                            <PagerStyle CssClass="pgr" />
                            <HeaderStyle ForeColor="White" />
                        </asp:GridView>


                        <table class="genTable" align="right">
                            <tr>
                                <td>
                                    <asp:Label ID="lbTemp" runat="server" Text="TEMP" Visible="false"></asp:Label></td>
                                <td>
                                    <asp:Label ID="lbtitiktemp" runat="server" Text=":" Visible="false"></asp:Label></td>
                                <td>
                                    <asp:TextBox ID="tbTemp" runat="server" Visible="false"></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ValidationGroup="ApproveValidation"
                                        ErrorMessage="* Must be fill" ControlToValidate="tbTemp"></asp:RequiredFieldValidator>

                                </td>

                                <td>ISSUED NOTE
                                </td>
                                <td>:</td>
                                <td>
                                    <asp:TextBox ID="tbIssuedNote" runat="server"></asp:TextBox>
                                </td>
                                <td>ISSUED BY</td>
                                <td>:</td>
                                <td>
                                    <asp:Label ID="lblIssBy" runat="server"></asp:Label>
                                </td>
                                <td>
                                    <asp:Button ID="btnSubmit" runat="server" Text="SUBMIT" CssClass="btnBlue"
                                        OnClick="btnSubmit_Click" ValidationGroup="ApproveValidation" />
                                </td>
                            </tr>
                        </table>

                    </asp:Panel>
                    <table>
                        <tr>
                            <td></td>
                        </tr>
                        <tr>
                            <td></td>
                        </tr>
                    </table>

                    <asp:Panel ID="PanelUser" runat="server" Visible="true" CssClass="genTable">
                        <asp:GridView ID="GVUser" runat="server" CssClass="mGrid"
                            AutoGenerateColumns="false" Width="100%"
                            AllowPaging="false" AllowSorting="True" ShowFooter="True" PageSize="10"
                            OnRowCommand="Gv_Detail_RowCommand"
                            OnRowDataBound="Gv_Detail_RowDataBound"
                            OnPageIndexChanging="Gv_Detail_PageIndexChanging" OnRowUpdated="GVUser_RowUpdated"
                            OnRowCancelingEdit="Gv_Detail_RowCancelingEdit"
                            OnRowEditing="Gv_Detail_RowEditing" OnRowUpdating="Gv_Detail_RowUpdating"
                            OnSorting="Gv_Detail_Sorting">
                            <PagerStyle CssClass="pgr" />
                            <HeaderStyle ForeColor="White" />
                            <Columns>
                                <asp:TemplateField HeaderText="No">
                                    <ItemTemplate>
                                        <asp:Label ID="lblNo" runat="server" CssClass="general"> </asp:Label>
                                    </ItemTemplate>
                                    <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" Width="4%" />
                                    <HeaderStyle HorizontalAlign="Center" />
                                </asp:TemplateField>

                                <asp:TemplateField HeaderText="ReqDetID" Visible="false">
                                    <ItemTemplate>
                                        <asp:Label ID="LblReqDetID" runat="server" CssClass="general" Text='<%# Bind("ReqDetID")%>'> </asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>

                                <asp:TemplateField HeaderText="ReqID" Visible="false">
                                    <ItemTemplate>
                                        <asp:Label ID="lblID" runat="server" CssClass="general" Text='<%# Bind("ReqID")%>'> </asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>

                                <asp:TemplateField HeaderText="SAP PART NO">
                                    <ItemTemplate>
                                        <asp:Label ID="lblitemsCode" runat="server" CssClass="general" Text='<%# Bind("partCode") %>'></asp:Label>
                                        <asp:Label ID="lbItemsID" runat="server" CssClass="general" Text='<%# Bind("partID")%>' Visible="false"> </asp:Label>
                                        <asp:Button ID="btnItemsCode" runat="server" CssClass="btnViewIcon" CommandName="ItemSearch" Visible="false" />

                                    </ItemTemplate>
                                    <EditItemTemplate>
                                        <asp:TextBox ID="TbItemsCode" runat="server" CssClass="general" Text='<%# Bind("partCode") %>' Width="120px"> </asp:TextBox>
                                        <asp:Label ID="lbItemsID" runat="server" CssClass="general" Text='<%# Bind("partID")%>' Visible="false"> </asp:Label>
                                        <asp:Button ID="btnItemsCode" runat="server" CssClass="btnViewIcon" CommandName="ItemSearch" />
                                    </EditItemTemplate>
                                    <FooterTemplate>
                                        <asp:TextBox ID="TbItemsCode" runat="server" CssClass="general" Width="120px"></asp:TextBox>
                                        <asp:Button ID="btnItemsCode" runat="server" CssClass="btnViewIcon" CommandName="ItemSearch" />
                                        <asp:Label ID="lbItemsID" runat="server" CssClass="general" Text='<%# Bind("itemsID")%>' Visible="false"> </asp:Label>
                                    </FooterTemplate>
                                    <ItemStyle HorizontalAlign="Left" Width="200px" />
                                    <HeaderStyle HorizontalAlign="Left" Width="200px" />
                                </asp:TemplateField>

                                <asp:TemplateField HeaderText="PART NAME" SortExpression="itemsName">
                                    <ItemTemplate>
                                        <asp:Label ID="LblitemsName" runat="server" CssClass="general" Text='<%# Bind("partName") %>'> </asp:Label>
                                    </ItemTemplate>
                                    <EditItemTemplate>
                                        <asp:Label ID="LblitemsName" runat="server" CssClass="general" Text='<%# Bind("partName") %>'> </asp:Label>
                                    </EditItemTemplate>
                                    <FooterTemplate>
                                        <asp:Label ID="LblitemsName" runat="server" CssClass="general"></asp:Label>
                                    </FooterTemplate>
                                </asp:TemplateField>

                                <asp:TemplateField HeaderText="MATERIAL" SortExpression="partNameAlias">
                                    <ItemTemplate>
                                        <asp:Label ID="LblpartNameAlias" runat="server" CssClass="general" Text='<%# Bind("partNameAlias") %>'> </asp:Label>
                                    </ItemTemplate>
                                    <EditItemTemplate>
                                        <asp:Label ID="LblpartNameAlias" runat="server" CssClass="general" Text='<%# Bind("partNameAlias") %>'> </asp:Label>
                                    </EditItemTemplate>
                                    <FooterTemplate>
                                        <asp:Label ID="LblpartNameAlias" runat="server" CssClass="general"></asp:Label>
                                    </FooterTemplate>
                                </asp:TemplateField>


                                <asp:TemplateField HeaderText="BATCH NO" SortExpression="lotNO" Visible="false">
                                    <ItemTemplate>
                                        <asp:Label ID="LblgoodsUnit" runat="server" CssClass="general" Text='<%# Bind("lotNO") %>'> </asp:Label>
                                    </ItemTemplate>
                                    <EditItemTemplate>
                                        <asp:Label ID="LblgoodsUnit" runat="server" CssClass="general" Text='<%# Bind("lotNO") %>'> </asp:Label>
                                    </EditItemTemplate>
                                    <FooterTemplate>
                                        <asp:Label ID="LblgoodsUnit" runat="server" CssClass="general"></asp:Label>
                                    </FooterTemplate>
                                </asp:TemplateField>

                                <asp:TemplateField HeaderText="QTY REQUEST">
                                    <ItemTemplate>
                                        <asp:Label ID="lbQtyRequest" runat="server" Text='<% #Bind("qtyRequest")%>'></asp:Label>
                                    </ItemTemplate>
                                    <EditItemTemplate>
                                        <asp:Label ID="lbQtyRequest" runat="server" Text='<% #Bind("qtyRequest")%>' Visible="false"></asp:Label>
                                        <asp:TextBox ID="EdQtyRequest" runat="server" Text='<% #Bind("qtyRequest")%>' CssClass="general" Width="50"></asp:TextBox>
                                    </EditItemTemplate>
                                    <FooterTemplate>
                                        <asp:TextBox ID="tbQtyRequest" runat="server" AutoPostBack="true" CssClass="general" Width="50"></asp:TextBox>
                                    </FooterTemplate>
                                </asp:TemplateField>

                                <%--                            <asp:TemplateField HeaderText="Status">
                                <ItemTemplate>
                                    <asp:HiddenField ID="hf_reqStatusId" runat="server" Value='<% #Bind("reqStatusID")%>' />
                                    <asp:Label ID="lbgoodsStatus" runat="server" Text='<% #Bind("reqStatusName")%>' CssClass="general"></asp:Label>
                                </ItemTemplate>
                                <EditItemTemplate>
                                    <asp:HiddenField ID="hfgoodsStatus" runat="server" Value='<% #Bind("reqStatusID")%>' />
                                    <asp:DropDownList ID="EdgoodsStatus" runat="server" CssClass="general"></asp:DropDownList>
                                </EditItemTemplate>
                                <FooterTemplate>
                                    <asp:DropDownList ID="ddgoodsStatus" runat="server" CssClass="general"></asp:DropDownList>
                                </FooterTemplate>
                            </asp:TemplateField>--%>

                                <asp:TemplateField HeaderText="QTY ISSUED">
                                    <ItemTemplate>
                                        <%--<asp:TextBox ID="tbQtyIssued" runat="server" Text='<% #Bind("qtyIssued") %>' CssClass="general"></asp:TextBox>--%>
                                        <asp:Label ID="lbQtyIssued" runat="server" Text='<% #Bind("qtyIssued")%>'></asp:Label>
                                    </ItemTemplate>

                                </asp:TemplateField>


                                <asp:TemplateField HeaderText="OUTSTANDING">
                                    <ItemTemplate>
                                        <%--<asp:TextBox ID="tbQtyIssued" runat="server" Text='<% #Bind("qtyIssued") %>' CssClass="general"></asp:TextBox>--%>
                                        <asp:Label ID="lbOutstanding" runat="server" Text='<% #Bind("balance")%>'></asp:Label>
                                    </ItemTemplate>

                                </asp:TemplateField>


                                <asp:TemplateField HeaderText="ISSUED NOTE">
                                    <ItemTemplate>
                                        <asp:Label ID="lbIssuedNote" runat="server" Text='<% #Bind("IssuedNote")%>'></asp:Label>
                                        <%--<asp:TextBox ID="tbIssuedNote" runat="server" Text='<% #Bind("IssuedNote") %>' CssClass="general"></asp:TextBox>--%>
                                    </ItemTemplate>
                                </asp:TemplateField>

                                <asp:TemplateField HeaderText="EDIT">
                                    <ItemTemplate>
                                        <asp:Button ID="btnViewEdit" runat="server" CausesValidation="False" CommandName="edit"
                                            CssClass="btnEditIcon" ToolTip="Edit" />
                                    </ItemTemplate>
                                    <EditItemTemplate>
                                        <asp:Button ID="btnViewUpdate" runat="server" CausesValidation="True"
                                            CommandName="Update" CssClass="btnSubmitIcon" ToolTip="Update" />
                                        <asp:Button ID="btnViewReject" runat="server" CausesValidation="true"
                                            CommandName="Cancel" CssClass="btnRejectIcon" ToolTip="Cancel" />
                                    </EditItemTemplate>
                                    <HeaderStyle HorizontalAlign="Center" />
                                    <ItemStyle HorizontalAlign="Center" VerticalAlign="Top" Width="8%" />
                                </asp:TemplateField>

                                <asp:TemplateField HeaderText="DELETE">
                                    <ItemTemplate>
                                        <asp:Button ID="btndelete" runat="server" CausesValidation="False" CommandName="del"
                                            CssClass="btnDeleteIcon" ToolTip="Delete" />
                                    </ItemTemplate>
                                    <FooterTemplate>
                                        <asp:Button ID="btnsave_RatShare" runat="server" CausesValidation="false" CommandName="save"
                                            CssClass="btnBlue" Text="Save" ToolTip="Save" />
                                    </FooterTemplate>
                                    <HeaderStyle HorizontalAlign="Center" />
                                    <FooterStyle HorizontalAlign="Center" />
                                    <ItemStyle HorizontalAlign="Center" VerticalAlign="Top" Width="7%" />
                                </asp:TemplateField>

                            </Columns>
                        </asp:GridView>



                        <%-------------------------------------------%>
                    </asp:Panel>

                    <%--------------------------------------------------------------------------------%>
                    <br />


                    <asp:Panel ID="panelSearch" runat="server" Visible="False" GroupingText="Asset Search" BackColor="#FFFFCC" CssClass="genTable">
                        <table class="genTable" width="100%">
                            <tr>
                                <td>Filter By&nbsp; &nbsp;
                            <asp:DropDownList ID="ddSearch" runat="server" Width="140px" Font-Names="Tahoma" CssClass="general"
                                Font-Size="8pt">
                                <asp:ListItem Value=""></asp:ListItem>
                                <asp:ListItem Value="A.itemsCode">SAP PART NO</asp:ListItem>
                                <asp:ListItem Value="A.itemsName">PART NAME</asp:ListItem>
                                <asp:ListItem Value="b.suppName">SUPPLIER</asp:ListItem>

                            </asp:DropDownList>&nbsp;
                        <asp:TextBox ID="tbSearch" runat="server" Width="200px" Font-Names="Tahoma"
                            Font-Size="8pt"> </asp:TextBox>
                                    &nbsp;&nbsp;&nbsp;
                            <asp:Button ID="btnSearch" runat="server" CssClass="btnBlue" Height="20px"
                                Text="Search" Width="70px" OnClick="btnSearch_Click" />
                                    &nbsp; &nbsp;
                                <asp:Button ID="Button1" runat="server" CssClass="btnBlue" Height="20px"
                                    OnClick="btnCancel_Click" Text="Close" Width="70px" />
                                </td>
                            </tr>

                        </table>
                        <asp:GridView ID="GVGoodslabelSearch" runat="server"
                            CssClass="mGrid" AutoGenerateColumns="false" AllowPaging="True" AllowSorting="True"
                            OnRowCommand="GVGoodslabelSearch_RowCommand" OnRowDataBound="GVGoodslabelSearch_RowDataBound"
                            OnPageIndexChanging="GVGoodslabelSearch_PageIndexChanging" OnSorting="GVGoodslabelSearch_Sorting">
                            <Columns>
                                <asp:TemplateField HeaderText="No">
                                    <ItemTemplate>
                                        <asp:Label ID="lblNo" runat="server"></asp:Label>
                                    </ItemTemplate>
                                    <ItemStyle Width="2%" />
                                </asp:TemplateField>

                                <asp:TemplateField HeaderText="itemsID" Visible="false">
                                    <ItemTemplate>
                                        <asp:Label ID="lbitemsID" Text='<% #Bind("itemsID")%>' runat="server"></asp:Label>
                                        <asp:Label ID="lbwhouseID" Text='<% #Bind("whouseID")%>' runat="server"></asp:Label>
                                        <asp:Label ID="lblsuppName" Text='<% #Bind("suppName")%>' runat="server"></asp:Label>
                                        <asp:Label ID="LblSuppID" Text='<% #Bind("suppID")%>' runat="server"></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>

                                <asp:TemplateField HeaderText="SAP PART NO">
                                    <ItemTemplate>
                                        <asp:Label ID="lbitemsCode" Text='<% #Bind("itemsCode")%>' runat="server"></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>

                                <asp:TemplateField HeaderText="PART NAME">
                                    <ItemTemplate>
                                        <asp:Label ID="lbitemsName" Text='<% #Bind("itemsName")%>' runat="server"></asp:Label>
                                    </ItemTemplate>
                                    <HeaderStyle HorizontalAlign="Left" />
                                </asp:TemplateField>

                                <asp:TemplateField HeaderText="Material">
                                    <ItemTemplate>
                                        <asp:Label ID="lbitemDesc" Text='<% #Bind("typeName")%>' runat="server"></asp:Label>
                                    </ItemTemplate>
                                    <HeaderStyle HorizontalAlign="Left" />
                                </asp:TemplateField>


                                <asp:TemplateField HeaderText="WAREHOUSE" SortExpression="whouseName">
                                    <ItemTemplate>
                                        <asp:Label ID="lbwhouseName" Text='<% #Bind("whouseName")%>' runat="server"></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>

                                <asp:TemplateField HeaderText="CURRENT QTY" SortExpression="currentQty">
                                    <ItemTemplate>
                                        <asp:Label ID="lbcurrentQty" Text='<% #Bind("nowStock")%>' runat="server"></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>


                                <asp:TemplateField HeaderText="BATCH NO" SortExpression="lotNO">
                                    <ItemTemplate>
                                        <asp:Label ID="lbgoodsUnit" Text='<% #Bind("lotNO")%>' runat="server"></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>

                                <asp:TemplateField HeaderText="QTY" SortExpression="SBQ">
                                    <ItemTemplate>
                                        <asp:Label ID="lbSBQ" Text='<% #Bind("SBQ")%>' runat="server"></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>

                                <asp:TemplateField HeaderText="PROJECT" SortExpression="projName">
                                    <ItemTemplate>
                                        <asp:Label ID="lbprojName" Text='<% #Bind("projName")%>' runat="server"></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>

                                <asp:TemplateField HeaderText="Add">
                                    <ItemTemplate>
                                        <asp:Button ID="Buttonadd" runat="server" CommandName="addGoods"
                                            CssClass="btnBlue" Text="Add" />
                                    </ItemTemplate>
                                    <ItemStyle HorizontalAlign="Center" />
                                    <HeaderStyle Width="3%" />
                                </asp:TemplateField>
                            </Columns>

                            <PagerStyle CssClass="pgr" />

                            <HeaderStyle ForeColor="White" />
                        </asp:GridView>
                    </asp:Panel>


                    <asp:HiddenField ID="hf_empno" runat="server" />
                    <asp:HiddenField ID="hf_empname" runat="server" />
                    <asp:HiddenField ID="hf_loginTime" runat="server" />
                    <asp:HiddenField ID="hf_deptID" runat="server" />
                    <asp:HiddenField ID="hf_HuroDeptID" runat="server" />
                    <asp:HiddenField ID="hf_userlevel" runat="server" />
                    <asp:HiddenField ID="hf_taskID" runat="server" />
                    <asp:HiddenField ID="hf_taskName" runat="server" />
                    <asp:HiddenField ID="hf_menuID" runat="server" />
                    <asp:HiddenField ID="hf_userID" runat="server" />
                    <asp:HiddenField ID="hf_dept" runat="server" />
                    <asp:HiddenField ID="hf_divisi" runat="server" />
                    <asp:HiddenField ID="hf_HuroDivisiID" runat="server" />
                    <asp:HiddenField ID="hf_Add" runat="server" />
                    <asp:HiddenField ID="hf_Update" runat="server" />
                    <asp:HiddenField ID="hf_delete" runat="server" />
                    <asp:HiddenField ID="hf_ro" runat="server" />
                    <asp:HiddenField ID="hf_AllDataBool" runat="server" />
                    <asp:HiddenField ID="hf_PageNo" runat="server" />
                    <asp:HiddenField ID="hf_modeView" runat="server" />
                    <asp:HiddenField ID="hf_ipaddress" runat="server" />
                    <asp:HiddenField ID="hf_projectTest" runat="server" />
                    <asp:HiddenField ID="hfappstep" runat="server" />
                    <asp:HiddenField ID="hf_Page" runat="server" />
                    <asp:HiddenField ID="hf_Row" runat="server" />
                    <asp:HiddenField ID="hf_ID" runat="server" />
                    <asp:HiddenField ID="hf_status" runat="server" />
                    <asp:HiddenField ID="hf_AppStep" runat="server" />
                    <asp:HiddenField ID="hf_tabIndex" runat="server" />
                    <asp:HiddenField ID="hf_Param1" runat="server" />
                    <asp:HiddenField ID="hf_Param2" runat="server" />
                    <asp:HiddenField ID="hf_SuperiorId" runat="server" />
                    <asp:HiddenField ID="hf_lastAppStep" runat="server" />
                    <asp:HiddenField ID="hf_onlineID" runat="server" />
                    <asp:HiddenField ID="hfParam1" runat="server" />
                    <asp:HiddenField ID="hfParam2" runat="server" />
                    <asp:HiddenField ID="hf_statusAttach" runat="server" />
                    <asp:HiddenField ID="hf_sts" runat="server" />
                    <asp:HiddenField ID="hf_TotalQty" runat="server" />
                    </div>
                </ContentTemplate>
            </asp:UpdatePanel>
            <asp:UpdateProgress ID="UProUP" runat="server" DynamicLayout="true" AssociatedUpdatePanelID="UpdatePanel1" DisplayAfter="0" EnableViewState="true">
                <ProgressTemplate>
                    <div id="Div1" align="center" valign="bottom" runat="server" class="genProgress">
                        <div id="Div2" align="center" valign="bottom" runat="server" class="genProgress2">
                            <img src="../../../Style/Image/Main/ProgressBar/loadingbar.gif" />
                            <br />
                            <span style="font-weight: bold">Loading</span>
                        </div>
                    </div>
                </ProgressTemplate>
            </asp:UpdateProgress>
        </div>


         <script type="text/javascript">
            var separCount = 0;
            var EnterCount = 0;
            var qrCodeScanner;
            qrCodeScanner = new Html5QrcodeScanner("qr-reader", {
                fps: 10, qrbox: 170, useBarCodeDetectorIfSupported: true
            });
            //$(document).ready(function () {


            qrCodeScanner.render(onScanSuccess);
            //});

            function onScanSuccess(decodedText, decodedResult) {

                const myArray = decodedText.split(";");
                let word = myArray[0];
                alert(word);
                CheckScanResult(word);
                //qrCodeScanner.clear();
            }

            $("#tbQR").keypress(function (e) {
                var dept = $("#tbQR").val();
                if (e.which == 13) {
                    EnterCount++;
                    if (EnterCount == 6) {
                        var mylabel = document.getElementById("lblName");
                        var str = $("#tbQR").val();


                        $("#lblName").text(str);
                        EnterCount = 0;

                        // __doPostBack("tbQR", "TextChanged");
                        CheckScanResult(str);
                        $("#tbQR").val("");


                    }
                    $("#lblEnterCount").text(EnterCount);
                    e.preventDefault();
                }
                else if (e.which == 59) {
                    separCount++;

                }
                else {
                }
            });

            function doback() {
                __doPostBack("Page", "Load");
            }

            function OnSuccess(response, userContext, methodName) {
                alert(response);
            }

            function CheckScanResult(QRText) {

            
                var myID = document.getElementById('<%= hf_ID.ClientID %>').value;
                var kode2 = document.getElementById('<%= tbQR.ClientID %>').value;
                  QRText = myID + ", " + "0" + ", " + QRText ;
                 //var myAllow = document.getElementById<%--('<%=cbAllow.ClientID%>')--%>.value; alert(document.getElementById<%--('<%= hf_ID.ClientID %>')--%>.value);
                //var x = $("#checkbox").is(":checked");
                //var x = document.getElementById("cbAllow").checked
                //alert('eeeeeeeeeeeeeee');
                 
                $.ajax({
                   // url: "PartRequestIssuedDetail.aspx/ScanData",
                  url: '<%= ResolveUrl("PartRequestIssuedDetail.aspx/ScanData") %>',

                    //async: true,
                    type: 'POST',
                    contentType: 'application/json',
                    data: JSON.stringify({param: QRText}),
                    cache: false,
                    dataType: 'json',
                    success: function (data) {
                        var myRes = data.d.split('|')
                        //alert(myRes[0]);
                        if (myRes[0]== '[SUCCESS]') {
                           // var cookName = 'lastParti' + myEventID;
                            //document.cookie = cookName + "=" + myRes[0] + "|" + EmpNo;
                          

                            alert(myRes[0]); //Success | [...] | [...]
                            //$("#lbStatus").text(data.d);
                            //document.getElementById('lbStatus').setAttribute('style', 'color: green;');

                            setTimeout(doback(), 3000);



                           //document.getElementById<%--('<%= hf_EventID.ClientID %>')--%>.setAttribute('style', 'color: green;');
                        }
                        else if (myRes[0]== '[EXISTING]') {
                          //  $("#lbStatus").text(data.d);
                            alert(myRes[0]);
                            //document.getElementById('lbStatus').setAttribute('style', 'color: orange;');
                         
                            //const img = document.getElementById('imgFoto');
                            //img.setAttribute('src', '');
                        }

                        //else if (myRes[0] == '[UNREGISTER]') {
                        else {
                            //$("#lbStatus").text(data.d);
                            //document.getElementById('lbStatus').setAttribute('style', 'color: red;');
                            alert(myRes[0] + "|" + "REGISTER PART FIRST");
                            //document.getElementById<%--('<%= hf_EventID.ClientID %>')--%>.setAttribute('style', 'color: red;');
                        }
                        //else alert('Unregistered...');



                    }, error: function (xhr, status, error) {
                        //var msg = JSON.parse(status);
                        alert('no data found '+error)

                        //document.getElementById('lbStatus').setAttribute('style', 'color: orange;');
                        //document.getElementByI<%--d('<%= hf_EventID.ClientID %>')--%>.setAttribute('style', 'color: yellow;');
                    },
                });
            }
        </script>


    </form>
</body>


</html>
