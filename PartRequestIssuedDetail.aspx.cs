﻿using Delogi.Form.Model.Main.Class;
using Delogi.Form.Model.Warehouse.Instance.Transaction;
using Delogi.Form.Model.Warehouse.Interface.Transaction;
using Delogi.Form.Model.Warehouse.Class.Transaction;
using Delogi.Form.Model.Warehouse.Instance.Master;
using Delogi.Form.Model.Warehouse.Interface.Master;
using Delogi.Form.Model.UserAdmin.Master.Instance;
using Delogi.Form.Model.UserAdmin.Master.Interface;
using Delogi.Form.Utils;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Telerik.Web.UI;
using System.Threading;
using System.IO;
using System.Web.Services;

namespace Delogi.Form.UI.Warehouse.Transaction
{
    public partial class PartRequestIssuedDetail : System.Web.UI.Page
    {
        GenControl pgc = new GenControl();
        GenMessage pgm = new GenMessage();
        GenMail gm = new GenMail();
        GenMailTemplate pmt = new GenMailTemplate();
        GenUploadDownload gud = new GenUploadDownload();
        ICommonApproval ica = InstApproval.GetInstanceCommonApproval;

        IDtGeneral instGeneral = InstWarehouseDT.GetDtGeneral;
        IDtIssued instiss = InstWarehouseDT.GetDtIssued;
        IDtIssuedDet instissdet = InstWarehouseDT.GetDtIssuedDet;
        IDtlabel instLabel = InstWarehouseDT.GetDtlabel;
        IDtIssuedWelderDet instisswelderdet = InstWarehouseDT.GetDtIssuedWelderDet;
        IMtItems instMtItem = InstWarehouseMT.GetMtItems;
        IMtWerehouse instwhouse = InstWarehouseMT.GetMtWerehouse;
        IMtProject instMtProject = InstWarehouseMT.GetMtProject;
        private string strUpdate = "Update";
        private string strSubmit = "Submit";
        private string strSave = "Save";
        private string strApprove = "Approve";

        #region Declarasi Variabel

        private string strBlank = "Blank";
        private string strTabNew = "NEW";
        private string strTabOP = "ONPROGRESS";
        private static string strTabHandle = "HANDLE";
        private string strTabComplete = "COMPLETE";
        private string strTabIssuedComplete = "IssuedComplete";
        private string strTabReject = "REJECT";

        private string classname = "Delogi.Form.UI.Warehouse.Transaction.PartRequestIssuedDetail";
        private object lblBalance;
        private string tbldtissued = "DtIssued";
        private string tbldtissuedDet = "DtIssuedDet";
        private string tbldtLabel = "DtLabel";
        private string tblmtItems = "MtItems"; 


        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {
            setRegisterControlUpdatePanelTrigger();
            if (!IsPostBack)
            {
                this.GetTableUserOnline();
                this.GetTableMenuDetail();

                GetDropDownData();
                if (!String.IsNullOrEmpty(hf_ID.Value))
                {
                    SetView();
                }
                else
                {
                    SetCreate();
                }

            }
        }
        private void GetTableUserOnline()
        {
            if (Request.QueryString["loginID"] != null)
            {
                string myLoginID = string.Empty;
                myLoginID = Request.QueryString["loginID"].ToString();
                OnlineID = myLoginID;

                UserOnline myLoginData = pgc.GetTableUserOnline(myLoginID);
                hf_loginTime.Value = myLoginData.LoginTime;
                hf_userlevel.Value = myLoginData.UserLevelID;
                hf_userID.Value = myLoginData.UserID;
                hf_empno.Value = myLoginData.EmployeeNo;
                hf_empname.Value = myLoginData.EmployeeName;
                hf_HuroDeptID.Value = myLoginData.DeptID;
                hf_HuroDivisiID.Value = myLoginData.DivisiID;
                hf_menuID.Value = myLoginData.MenuID;
                /*Select IDOnline,SessionID,IPAddress,MenuID,MenuName,MenuUrl,UserLevelID,UserLevelName,AppID,AppName,AppDeptID,AppDeptName,UserID,EmployeeName,EmployeeNo,AccountName,Office,FullName,
                 * Email,DivisiID,DivisiName,DeptID,DeptName,SectionID,SectionName,LineID,LineName,Posisi,LevelEmployee,Status,LoginTime*/
            }


        }
        private string itemsID
        {
            get { return hf_ID.Value; }
            set
            {
                hf_ID.Value = value;
            }
        }
        private void GetTableMenuDetail()
        {
            MenuDetail myMenuDetail = pgc.GetTableMenuDetail(OnlineID);
            hf_Add.Value = myMenuDetail.AddData;
            hf_Update.Value = myMenuDetail.UpdateData;
            hf_delete.Value = myMenuDetail.DeleteData;
            hf_ro.Value = myMenuDetail.ROData;
            hf_AllDataBool.Value = myMenuDetail.AllDataBool;
            hf_tabIndex.Value = myMenuDetail.ActiveTab;

            if (!string.IsNullOrEmpty(hf_tabIndex.Value.Trim()))
            {
                if(hf_tabIndex.Value == "0")
                {
                    hf_modeView.Value = strTabNew.ToLower();
                }
                else if (hf_tabIndex.Value == "1")
                {
                    hf_modeView.Value = strTabOP.ToLower();
                }
                else if (hf_tabIndex.Value == "2")
                {
                    hf_modeView.Value = strTabHandle.ToLower();
                }
                else if (hf_tabIndex.Value == "3")
                {
                    hf_modeView.Value = strTabComplete.ToLower();
                }
                else
                {
                    hf_modeView.Value = strTabIssuedComplete.ToLower();
                }
            }

            if (!string.IsNullOrEmpty(myMenuDetail.QueryString))
            {
                NameValueCollection qscoll = HttpUtility.ParseQueryString(myMenuDetail.QueryString);
                Hashtable ht = new Hashtable();
                foreach (string item in qscoll)
                {
                    if (item.Trim() == "appInID")
                    {
                        itemsID = qscoll[item].ToString().Trim();
                        break;
                    }
                    
                }
            }
            else
            {
                this.ResponseRedirectPage();
            }
        }

        private void ResponseRedirectPage()
        {
            pgc.SetGeneralSearching(OnlineID, hf_AllDataBool.Value, hf_tabIndex.Value);
            Response.Redirect("PartRequestIssued.aspx?&LoginID=" + OnlineID);
        }
        private string OnlineID
        {
            get { return hf_onlineID.Value; }
            set
            {
                hf_onlineID.Value = value;
            }
        }

        #region BindData

        private void bindDropDown(DataTable dttemp, string dataText, string valueText, DropDownList dd)
        {
            dd.DataSource = dttemp;
            dd.DataTextField = dataText;
            dd.DataValueField = valueText;
            dd.DataBind();
            ListItem litem = new ListItem();
            litem.Text = "";
            litem.Value = "0";
            dd.Items.Insert(0, litem);
        }
        private void bindDropDownnull(DataTable dttemp, string dataText, string valueText, DropDownList dd)
        {
            dd.DataSource = dttemp;
            dd.DataTextField = dataText;
            dd.DataValueField = valueText;
            dd.DataBind();
        }
        
        private void BindWhouse()
        {
            DataTable dttemp = instwhouse.GetMtWerehouse();
            bindDropDown(dttemp, "whouseName", "whouseID", ddwhouse);
        }

        protected void ddWHouseTo_SelectedIndexChanged(object sender, EventArgs e)
        {
            BindProject(ddwhouse.SelectedValue.ToString());
        }

        private void BindProject(string whouseID)
        {
            DataTable dtProj = instMtProject.GetMtProject(new GenClassCommonParamList { ManualQuery = " and a.whouseID =" + whouseID });
            if (dtProj.Rows.Count >= 1)
            {
                bindDropDown( dtProj, "projName", "projID", ddProject);
            }
        }
       

        #region Utilities
        private void SetEnableObject(bool Enable)
        {
            pgc.setControlEnabled(Enable, ddProject, tbReason, ddwhouse/*, tbStampNo*/);
        }

        private void cekProjectTesting()
        {
            if (hf_projectTest.Value == "1" || hf_projectTest.Value == "True")
            {
                visibleTemp(true);
            }
            else
            {
                visibleTemp(false);
            }

        }

        private void visibleTemp(bool truefalse)
        {
            tbTemp.Visible = truefalse;
            lbTemp.Visible = truefalse;
            lbtitiktemp.Visible = truefalse;
        }

        #endregion

        private void SetCreate()
        {
            panelAdmin.Visible = false;
            PanelUser.Visible = true;
            pgc.setControlVisible(false, Btnupdate, btnCancel, btnRequest);
            btnSave.Visible = true;
            btnSave.Text = strSave;
            
            BindWhouse();
            setVisible(false);
        }
        private void SetView()
        {
            setVisible(true);
            BindWhouse();
            pgc.setControlVisible(false, btnSave, btnCancel);
            Btnupdate.Visible = true;
            SetEnableObject(false);
            DataTable dtCurrentHead = instiss.GetDtIssued(setParameter(tbldtissued));
            if (dtCurrentHead.Rows.Count > 0)
            {
                lbl_ReqID.Text = hf_menuID.Value + "-" + hf_ID.Value.ToString();
                lblRequestDate.Text = dtCurrentHead.Rows[0]["req_Date"].ToString();
                lblRequestBy.Text = dtCurrentHead.Rows[0]["Req_byName"].ToString();
                lblinsert_by.Text = dtCurrentHead.Rows[0]["Req_by"].ToString();
                ddwhouse.SelectedValue = dtCurrentHead.Rows[0]["whouseID"].ToString();
                tbReason.Text = dtCurrentHead.Rows[0]["reason"].ToString();
                tbTemp.Text = dtCurrentHead.Rows[0]["temp"].ToString().Replace(",", "."); ;
                hf_projectTest.Value = dtCurrentHead.Rows[0]["isTest"].ToString();
                hf_AppStep.Value = dtCurrentHead.Rows[0]["status"].ToString();
                string isSubmit = dtCurrentHead.Rows[0]["isSubmit"].ToString();
                if(isSubmit == "0" || string.IsNullOrEmpty(isSubmit)|| isSubmit == "False")
                {
                    hf_AppStep.Value = "0";
                }
                cekProjectTesting();
                BindProject(ddwhouse.SelectedValue.ToString());
                string projID = dtCurrentHead.Rows[0]["projID"].ToString();
                ddProject.SelectedValue = projID.Trim();
                DataTable dtIssuedDet = instissdet.GetDtIssuedDet(setParameter(tbldtissuedDet));
                BindDataGV(GVUser, dtIssuedDet);
                lblIssBy.Text = hf_empname.Value.ToString();

                tbIssuedNote.Text = dtCurrentHead.Rows[0]["Comment"].ToString();
            }

            if (hf_modeView.Value.ToString().ToLower() == strTabNew.ToLower())
            {
                btnRequest.Visible = true;
                visibleTemp(false);
                btnSubmit.Visible = false;
                tbIssuedNote.Visible = false;
            }
            else if (hf_modeView.Value.ToString().ToLower() == strTabOP.ToLower())
            {
                panelAdmin.Visible = true;
                btnSubmit.Visible = false;
                Btnupdate.Visible = false;

            }
            else if (hf_modeView.Value.ToString().ToLower() == strTabHandle.ToLower())
            {
                panelAdmin.Visible = true;
                Btnupdate.Visible = false;
                if (hf_projectTest.Value == "1" || hf_projectTest.Value == "True")
                {
                    tbTemp.Text = "";
                }
                if (hf_AppStep.Value == "1")
                {
                    btnRequest.Visible = true;
                    btnRequest.Text = "Propose";
                    btnSubmit.Visible = false;
                    panelAdmin.Visible = false;
                }
                else if (hf_AppStep.Value == "2")
                {
                    btnRequest.Visible = true;
                    btnRequest.Text = "Store";
                    btnSubmit.Visible = false;
                    panelAdmin.Visible = false;
                }
                else if (hf_AppStep.Value == "3")
                {
                    panelQRHandle.Visible = true;
                }
                    

                DataTable dtadminissdet = instissdet.GetDtIssuedDet(setParameter(tbldtLabel), "ListIssued2");
                BindDataGV(GVAdmin, dtadminissdet);

            }
            else if (hf_modeView.Value.ToString().ToLower() == strTabComplete.ToLower())
            {
                panelAdmin.Visible = true;
                btnSubmit.Visible = false;
                Btnupdate.Visible = false;

                DataTable dtadminissdet = instissdet.GetDtIssuedDet(setParameter(tbldtLabel), "ListIssued2");
                BindDataGV(GVAdmin, dtadminissdet);
            }

            setSuperior();

        }

        private void BindDataGV(GridView gv, DataTable dt)
        {
            dt.Columns.Add("No");
            if (dt.Rows.Count < 1)
            {
                DataRow dr = dt.NewRow();
                dr["No"] = "1";

                dt.Rows.Add(dr);
            }

            gv.DataSource = dt;
            gv.DataBind();
            setRegisterControlUpdatePanelTrigger();
            pgc.setGVCaptionTotal(gv, dt.Rows.Count.ToString());
            dt.Dispose();

        }
        

        private void setEnable(bool TrueOrFalse)
        {
            //pgc.setControlEnabled(TrueOrFalse, btNew, BtActive);

        }

        private void setVisible(bool TrueOrFalse)
        {
            //pgc.setControlVisible(TrueOrFalse, btNew, BtActive);

        }

        private GenClassCommonParamList setParameter(string tableName)
        {
            GenClassCommonParamList pcp = new GenClassCommonParamList();

            if (!string.IsNullOrEmpty(hf_ID.Value))
            {
                pcp.ParamField1 = "a.ReqID";
                pcp.Operrator1 = "=";
                pcp.ParamFieldData1 = hf_ID.Value.Trim();

                //if (tableName== tbldtissuedDet)
                //{

                //}
                
            }
            //else if (tableName == tblmtProject)
            //{
            //    pcp.ManualQuery = " and a.whouseID =" + ddwhouse.SelectedValue;

            //}


            return pcp;
        }

        private DataTable GetDatatable(bool search, string tblName, GenClassCommonParamList pcp)
        {
            string menuID = hf_menuID.Value;
            DataTable dtTemp = new DataTable();
            string mySession = classname + tblName + hf_empno.Value + hf_loginTime.Value;
            try
            {
                if ((!IsPostBack && !IsExistsSession(mySession)) || search || !IsExistsSession(mySession))
                {
                    if (tblName == tbldtissued)
                    {
                        dtTemp = instiss.GetDtIssued(pcp);
                    }
                    else if (tblName == tbldtissuedDet)
                    {
                        pcp.ParamField1 = "a.ReqID";
                        pcp.Operrator1 = "=";
                        pcp.ParamFieldData1 = hf_ID.Value.Trim();

                        dtTemp = instissdet.GetDtIssuedDet(pcp);
                    }
                    this.SetDataSession(dtTemp, mySession);
                }
                else
                {
                    if (Session[mySession] != null)
                    {
                        dtTemp = (DataTable)Session[mySession];
                    }
                    if (dtTemp.Rows.Count < 1)
                    {
                        this.GetDatatable(true, tblName, pcp);
                    }
                }

            }
            catch (Exception ex) { }
            if (dtTemp.Rows.Count == 0)
                dtTemp.Rows.Add();

            return dtTemp;
        }

        private void GetDropDownData()
        {

            //DataTable dtInstitu = GetDatatable(true, listInstitu, setParameter(listInstitu));
            //BindDropDown(dtInstitu, ddInstitu, "educInstitu", "educInstitu");


        }
        //private void BindDropDown(DataTable dtTemp, DropDownList mydd, string mytext, string myvalue)
        //{
        //    if (dtTemp.Rows.Count > 0)
        //    {
        //        mydd.DataSource = dtTemp;
        //        mydd.DataValueField = myvalue;
        //        mydd.DataTextField = mytext;
        //        mydd.DataBind();
        //    }
        //}

        private void BindHistApproval()
        {
            //DataTable dt = instAgree.GetAgree_dtApprovalHistory(hf_ID.Value);
            //gvHistoryApproval.DataSource = dt;
            //gvHistoryApproval.DataBind();
        }

        #endregion BindData

        #region SessionTab
        private void SetSession()
        {
            Session["dept_huro"] = hf_dept.Value;
            Session["divisi"] = hf_divisi.Value;
            Session["ipaddress"] = hf_ipaddress.Value;

            if (Session["LoginTime"] != null)
                Session.Remove("LoginTime");
            Session["LoginTime"] = hf_loginTime.Value.Trim();
            if (Session["user_level_id"] != null)
                Session.Remove("user_level_id");
            Session["user_level_id"] = hf_userlevel.Value.Trim();
            if (Session["user_id"] != null)
                Session.Remove("user_id");
            Session["user_id"] = hf_userID.Value.Trim();
            if (Session["deptid"] != null)
                Session.Remove("deptid");
            Session["deptid"] = hf_deptID.Value.Trim();
            if (Session["deptName"] != null)
                Session.Remove("deptName");
            if (Session["employee_no"] != null)
                Session.Remove("employee_no");
            Session["employee_no"] = hf_empno.Value.Trim();
            if (Session["employee_name"] != null)
                Session.Remove("employee_name");
            Session["employee_name"] = hf_empname.Value.Trim();
            if (Session["menu_ID"] != null)
                Session.Remove("menu_ID");
            Session["menu_ID"] = hf_menuID.Value.Trim();
            if (Session["HURODeptID"] != null)
                Session.Remove("HURODeptID");
            Session["HURODeptID"] = hf_HuroDeptID.Value.Trim();
            if (Session["HUROdivisiID"] != null)
                Session.Remove("HUROdivisiID");
            Session["HUROdivisiID"] = hf_HuroDivisiID.Value.Trim();

        }
        private void SetDataSession(DataTable dtTemp, string sessioName)
        {

            if (Session[sessioName] != null)
            {
                Session.Remove(sessioName);
            }
            Session[sessioName] = dtTemp;
        }
        private void SetStringSession(string strTemp, string sessioName)
        {
            if (Session[sessioName] != null)
            {
                Session.Remove(sessioName);
            }
            Session[sessioName] = strTemp;
        }
        private bool IsExistsSession(string sessionName)
        {
            if (Session[sessionName] != null)
                return true;
            else
                return false;
        }
        public override void VerifyRenderingInServerForm(Control control)
        {
            return;
        }
        private void setRegisterControlUpdatePanelTrigger()
        {
            ScriptManager sm = ScriptManager.GetCurrent(this.Page);
            sm.RegisterPostBackControl(btnClose);
            sm.RegisterPostBackControl(btnClose);
            sm.RegisterPostBackControl(btnSave);
            sm.RegisterPostBackControl(Btnupdate);
            sm.RegisterPostBackControl(btnCancel);
            sm.RegisterPostBackControl(btnRequest);
            sm.RegisterPostBackControl(btnSearch);
            sm.RegisterPostBackControl(Button1);
            sm.RegisterPostBackControl(btnSubmit);
        }
        private void GetSession()
        {
            if (Session["LoginTime"] != null)
                hf_loginTime.Value = Session["LoginTime"].ToString();
            if (Session["user_level_id"] != null)
                hf_userlevel.Value = Session["user_level_id"].ToString();
            if (Session["user_id"] != null)
                hf_userID.Value = Session["user_id"].ToString();
            if (Session["deptid"] != null)
                hf_deptID.Value = Session["deptid"].ToString();
            if (Session["employee_no"] != null)
                hf_empno.Value = Session["employee_no"].ToString();
            if (Session["employee_name"] != null)
                hf_empname.Value = Session["employee_name"].ToString();
            if (Session["HURODeptID"] != null)
                hf_HuroDeptID.Value = Session["HURODeptID"].ToString();
            if (Session["HUROdivisiID"] != null)
                hf_HuroDivisiID.Value = Session["HUROdivisiID"].ToString();
            if (Session["menu_ID"] != null)
                hf_menuID.Value = Session["menu_ID"].ToString();
            if (Session["dept_huro"] != null)
                hf_dept.Value = Session["dept_huro"].ToString();
            if (Session["divisi"] != null)
                hf_divisi.Value = Session["divisi"].ToString();
            if (Session["ipaddress"] != null)
                hf_ipaddress.Value = Session["ipaddress"].ToString();

            if (Session["var"] != null)
            {
                //   Tab.ActiveTabIndex = pgc.safeInt(Session["var"].ToString());
                Session.Remove("var");
            }
        }
        private void GetSessionTest()
        {
            hf_loginTime.Value = DateTime.Now.ToString();
            hf_userlevel.Value = "1";
            hf_userID.Value = "388";
            hf_deptID.Value = "1";
            hf_empno.Value = "2160071";
            hf_empname.Value = "Indriani Dewi Safitri";
            hf_HuroDeptID.Value = "4";
            hf_HuroDivisiID.Value = "5";
            hf_menuID.Value = "283";
            hf_dept.Value = "ISD";
            hf_divisi.Value = "PPnC";
        }
        private void SetTabSession(int myActiveTab, string cari, string value, string tanggal, string dari, string sampai, string sessioName, string page)
        {
            try
            {
                if (Session[sessioName] != null)
                {
                    Session.Remove(sessioName);
                }
                Session.Remove(sessioName);
                Session[sessioName] = myActiveTab + "|" + cari + "|" + value + "|" + tanggal + "|" + dari + "|" + sampai + "|" + page;
            }
            catch (Exception ex)
            {
                //this.//PGL.(ex, "SetDataSession");
            }
        }

        private string textCut(string text, int nilai)
        {
            int i = nilai - 1;
            string[] txt = null;

            try
            {
                txt = text.Split(new Char[] { '|' });
                return txt[i];
            }
            catch
            {
                return txt[0];
            }
        }

        #endregion

        

        #region gridviewPage
        private string GridViewSortDirection
        {
            get { return ViewState["SortDirection"] as string ?? "DESC"; }
            set { ViewState["SortDirection"] = value; }
        }
        private string GridViewSortExpression
        {
            get { return ViewState["SortExpression"] as string ?? string.Empty; }
            set { ViewState["SortExpression"] = value; }
        }
        private void setSorting(GridView gv, object sender, GridViewSortEventArgs e, DataTable dtTemp)
        {
            GridViewSortExpression = e.SortExpression;
            this.pgc.GVSortExpression = GridViewSortExpression;
            this.pgc.GVSortDirection = GridViewSortDirection;
            int pageIndex = gv.PageIndex;
            gv.DataSource = this.pgc.SortDataTable(dtTemp, false);
            GridViewSortDirection = this.pgc.GVSortDirection;
            gv.DataBind();
            gv.PageIndex = pageIndex;
        }
        private void setPaging(GridView gv, object sender, GridViewPageEventArgs e, DataTable dtTemp)
        {
            gv.SelectedIndex = -1;
            this.pgc.GVSortExpression = GridViewSortExpression;
            this.pgc.GVSortDirection = GridViewSortDirection;
            gv.DataSource = this.pgc.SortDataTable(dtTemp, true);
            gv.PageIndex = e.NewPageIndex;
            gv.DataBind();

        }
        private void setRowsNum(GridView gv, string numrows, DataTable dtTemp)
        {
            int data = 0;
            int data1 = 0;
            data = this.pgc.safeInt(numrows);
            data1 = dtTemp.Rows.Count;
            if (data > 0)
            {
                if (data1 > 0)
                {
                    if (data1 < data)
                        data = data1;
                    gv.DataSource = pgc.SortDataTable(dtTemp, true);
                    gv.PageSize = data;
                    gv.DataBind();
                }
            }
        }
        private void setPageIndex(GridView gv, string indeks, DataTable dtTemp)
        {
            int indek = 0;
            int data1 = 0;
            int data2 = 0;
            indek = this.pgc.safeInt(indeks);
            if (indek <= 0)
                indek = 1;
            indek = indek - 1;
            data1 = dtTemp.Rows.Count;
            if (data1 > 0)
            {
                data2 = gv.PageCount;
                if (indek < 0 || indek > data2)
                    indek = data2;
                gv.DataSource = pgc.SortDataTable(dtTemp, true);
                gv.PageIndex = indek;
                gv.DataBind();
            }
        }

        #endregion

        #region DETAIL

        protected void Gv_Detail_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                Label lblno = (Label)e.Row.FindControl("lblNo");
                lblno.Text = Convert.ToString(e.Row.RowIndex + 1 + (GVUser.PageIndex * GVUser.PageSize));

                Button btnitemsCode = (Button)e.Row.FindControl("btnitemsCode");
                if (hf_modeView.Value.ToString().ToLower() != strTabNew.ToLower())
                {
                    btnitemsCode.Visible = false;
                    GVUser.Columns[11].Visible = false;
                    GVUser.Columns[12].Visible = false;
                    GVUser.ShowFooter = false;
                }


            }
        }

        protected void Gv_Detail_RowCommand(object sender, GridViewCommandEventArgs e)
        {

            Control source = e.CommandSource as Control;
            GridViewRow row = source.NamingContainer as GridViewRow;
            panelSearch.Visible = false;

            if (e.CommandName == "save")
            {
                Label lbItemsID = (Label)GVUser.FooterRow.FindControl("lbItemsID");
                TextBox tbQtyRequest = (TextBox)GVUser.FooterRow.FindControl("tbQtyRequest");

                DtIssuedDet item = new DtIssuedDet();
                item.ReqID = hf_ID.Value.ToString();
                item.ItemsID = pgc.safeString(lbItemsID.Text.Trim());
                item.QtyRequest = pgc.safeString(tbQtyRequest.Text.Trim());
                item.QtyIssued = "0";

                int isExist = 0;
                for (int i = 0; i < GVUser.Rows.Count; i++)
                {
                    Label lblpartID = (Label)GVUser.Rows[i].FindControl("lbItemsID");
                    if (lblpartID.Text.Trim() == lbItemsID.Text.Trim())
                    {
                        isExist = 1;
                    }
                }
                if (isExist == 0)
                {

                    int save = 0;
                    save = instissdet.SaveDtIssuedDet(item);

                    if (save > 0)
                    {
                        pgm.MessageBoxAjaxs(UpdatePanel1, pgm.msgSaved(), false);
                        BindDataGV(GVUser, GetDatatable(true, tbldtissuedDet, new GenClassCommonParamList()));
                        hf_sts.Value = "";
                    }
                    else
                    {
                        pgm.MessageBoxAjaxs(UpdatePanel1, pgm.msgSavedFail(), false);
                    }
                }
                else
                {
                    pgm.MessageBoxAjaxs(UpdatePanel1, "Part Already Exist !", false);
                }

            }
            else if (e.CommandName == "Update")
            {

            }
            else if (e.CommandName == "del")
            {
                Label LblReqDetID = ((Label)GVUser.Rows[row.RowIndex].FindControl("LblReqDetID"));

                List<GenClassIdentity> mylist = new List<GenClassIdentity>();
                GenClassIdentity pID = new GenClassIdentity();
                pID.ID = LblReqDetID.Text.ToString();
                mylist.Add(pID);

                bool d = instissdet.DeleteByDtIssuedDetID(hf_empno.Value.ToString(), mylist);
                if (d == true)
                {
                    pgm.MessageBoxAjaxs(UpdatePanel1, pgm.msgDeleted(), false);
                    DataTable dtIssuedDet = instissdet.GetDtIssuedDet(setParameter(tbldtissued));
                    BindDataGV(GVUser, dtIssuedDet);
                }
                else
                {
                    pgm.MessageBoxAjaxs(UpdatePanel1, pgm.msgDeletedFail(), false);
                }

            }

            if (e.CommandName == "ItemSearch")
            {
                panelSearch.Visible = true;
            }
            else if (e.CommandName == "edit")
            {
                GVUser.ShowFooter = false;
            }
        }

        protected void Gv_Detail_RowEditing(object sender, GridViewEditEventArgs e)
        {
            GVUser.EditIndex = e.NewEditIndex;
            DataTable dt = instissdet.GetDtIssuedDet(setParameter(tbldtissuedDet));
            BindDataGV(GVUser, dt);
            hf_sts.Value = "edit";
            hf_Row.Value = (e.NewEditIndex).ToString();
            GVUser.ShowFooter = false;
        }

        protected void Gv_Detail_RowUpdating(object sender, GridViewUpdateEventArgs e)
        {
            if (hf_Update.Value == "1")
            {
                int i = Convert.ToInt32(hf_Row.Value);
                string uang = string.Empty;
                Label LblReqDetID = (Label)GVUser.Rows[i].FindControl("LblReqDetID");
                Label LblReqID = (Label)GVUser.Rows[i].FindControl("lblID");
                TextBox EdQtyRequest = (TextBox)GVUser.Rows[i].FindControl("EdQtyRequest");
                Label lbItemsID = (Label)GVUser.Rows[i].FindControl("lbItemsID");

                DtIssuedDet item = new DtIssuedDet();
                item.ReqDetID = LblReqDetID.Text.ToString().Trim();
                item.ReqID = hf_ID.Value.ToString();
                item.ItemsID = pgc.safeString(lbItemsID.Text.Trim());
                item.QtyRequest = pgc.safeString(EdQtyRequest.Text.Trim());
                item.QtyIssued = "0";

                bool update = instissdet.UpdateDtIssuedDet(item);

                if (update)
                {
                    pgm.MessageBoxAjaxs(UpdatePanel1, pgm.msgUpdated(), false);
                    GVUser.EditIndex = -1;
                    this.GVUser.ShowFooter = true;
                    BindDataGV(GVUser, GetDatatable(true, tbldtissuedDet, new GenClassCommonParamList()));
                    hf_sts.Value = "";
                    GVUser.ShowFooter = true;
                }
                else
                {
                    pgm.MessageBoxAjaxs(UpdatePanel1, pgm.msgUpdatedFail(), false);
                }

            }
        }

        protected void Gv_Detail_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
        {
            GVUser.EditIndex = -1;
            this.GVUser.ShowFooter = true;
            BindDataGV(GVUser, GetDatatable(true, tbldtissuedDet, new GenClassCommonParamList()));
            hf_sts.Value = "";
            GVUser.ShowFooter = true;
        }

        protected void Gv_Detail_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            this.setPaging(((GridView)sender), sender, e, GetDatatable(false, tbldtissuedDet, setParameter(tbldtissuedDet)));
        }

        protected void Gv_Detail_Sorting(object sender, GridViewSortEventArgs e)
        {
            this.setSorting(((GridView)sender), sender, e, GetDatatable(false, tbldtissuedDet, setParameter(tbldtissuedDet)));
        }

        #endregion

        #region GV_Asset_Move_Detail

        protected void GvAssetMove_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName != "Page" && e.CommandName != "Sort")
            {
                Control source = e.CommandSource as Control;
                GridViewRow row = source.NamingContainer as GridViewRow;
                hf_Row.Value = row.RowIndex.ToString();

                if (e.CommandName == "SearchView")
                {
                    //panelSearch.Visible = true;

                }
                else if (e.CommandName == "addDetail")
                {
                    if (hf_ID.Value.ToString() != string.Empty)
                    {


                    }

                }
                else if (e.CommandName == "delete")
                {

                }
            }
        }

        private bool SaveUpdateHeader()
        {
            bool result = false;
            if (ddwhouse.SelectedItem.Text != string.Empty)
            {
                DtIssued itemHeader = new DtIssued();
                itemHeader.Insert_by = hf_empno.Value.ToString().Trim();
                itemHeader.Reason = pgc.safeString(tbReason.Text);
                itemHeader.Req_by = hf_empno.Value.ToString();
                itemHeader.WhouseID = ddwhouse.SelectedValue.ToString();
                itemHeader.ProjID = ddProject.SelectedValue.ToString();
                itemHeader.Req_date = DateTime.Now.ToString("yyyy-MM-dd");
                itemHeader.Temp = "0";// tbTemp.Text.Replace(",", "."); ;

                #region Saving
                if (hf_ID.Value == "" || hf_ID.Value == "0")
                {

                    int saving = instiss.SaveDtIssued(itemHeader);
                    if (saving > 0)
                    {
                        pgm.MessageBoxAjaxs(this.UpdatePanel1, this.pgm.msgSaved(), false);
                        result = true;
                        hf_ID.Value = saving.ToString();
                        hf_modeView.Value = strTabNew;
                        SetEnableObject(false);
                    }
                    else
                    {

                        pgm.MessageBoxAjaxs(this.UpdatePanel1, this.pgm.msgSavedFail(), false);
                        SetEnableObject(true);
                    }

                }
                #endregion

                #region Update
                else
                {
                    itemHeader.ReqID = hf_ID.Value.ToString();
                    result = instiss.UpdateDtIssued(itemHeader);
                    if (result)
                    {
                        //this.pgm.MessageBoxAjaxs(this.UpdatePanel1, this.pgm.msgUpdated(), false);
                    }
                    else
                    {
                        //this.pgm.MessageBoxAjaxs(this.UpdatePanel1, this.pgm.msgUpdatedFail(), false);
                    }
                }
                #endregion
            }
            if (result)
            {
                btnSave.Text = "Update";
                //bindToForm();
                SetEnableObject(false);
                //this.pgm.MessageBoxAjaxs(UpdatePanel1, "Data has been saved", false);
            }
            return result;
        }


        decimal DecimalTotalScrapWeightsum = 0;
        protected void GvAssetMove_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                Label lblNumber = (Label)e.Row.FindControl("lblNo");
                lblNumber.Text = Convert.ToString((e.Row.RowIndex + 1) + (((GridView)sender).PageIndex * ((GridView)sender).PageSize));

                Button btnDel = (Button)e.Row.FindControl("ButtonDelete");
                if (hf_modeView.Value.ToString().ToLower() == strBlank.ToLower() || hf_modeView.Value.ToString().ToLower() == strTabNew.ToLower())
                {
                    btnDel.Enabled = true;
                }
                else
                {
                    btnDel.Enabled = false;
                }

                Label lblweightQty = (Label)e.Row.FindControl("lblweightQty");
                if (hf_projectTest.Value == "1" || hf_projectTest.Value == "True")
                {

                    GVUser.Columns[12].Visible = true;
                    lblweightQty.Visible = true;
                }
                else
                {
                    GVUser.Columns[12].Visible = false;
                    lblweightQty.Visible = false;
                }
                
                if (lblweightQty.Text != "")
                {
                    decimal DecimalTotalScrapWeight = Convert.ToDecimal(DataBinder.Eval(e.Row.DataItem, "weightQty"));
                    DecimalTotalScrapWeightsum += DecimalTotalScrapWeight;
                    hf_TotalQty.Value = DecimalTotalScrapWeightsum.ToString();
                }

            }

            if (e.Row.RowType == DataControlRowType.Footer)
            {
                Label TotalScapWeight = (Label)e.Row.FindControl("lbltotalQty");
                TotalScapWeight.Font.Bold = true;
                TotalScapWeight.Text = "Total : " + hf_TotalQty.Value;
            }
        }
        protected void GvAssetMove_Sorting(object sender, GridViewSortEventArgs e)
        {

        }
        protected void GvAssetMove_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {

        }
        #endregion

        #region GVSearch
        protected void GVGoodslabelSearch_RowCommand(object sender, GridViewCommandEventArgs e)
        {

            Control source = e.CommandSource as Control;
            GridViewRow row = source.NamingContainer as GridViewRow;

            if (e.CommandName == "addGoods")
            {
                int vindeks = 0;
                vindeks = Convert.ToInt32(row.RowIndex.ToString().Trim());
                string itemsID = ((Label)GVGoodslabelSearch.Rows[vindeks].FindControl("lbitemsID")).Text.ToString();
                string itemsCode = ((Label)GVGoodslabelSearch.Rows[vindeks].FindControl("lbitemsCode")).Text.ToString();
                string itemsName = ((Label)GVGoodslabelSearch.Rows[vindeks].FindControl("lbitemsName")).Text.ToString();
                string goodsUnit = ((Label)GVGoodslabelSearch.Rows[vindeks].FindControl("lbgoodsUnit")).Text.ToString();
                string material = ((Label)GVGoodslabelSearch.Rows[vindeks].FindControl("lbitemDesc")).Text.ToString();


                if (hf_sts.Value.ToString().Trim() == "edit")
                {
                    Label lbitemsID = (Label)GVUser.Rows[Convert.ToInt32(hf_Row.Value.ToString())].FindControl("lbItemsID");
                    TextBox tbitemsCode = (TextBox)GVUser.Rows[Convert.ToInt32(hf_Row.Value.ToString())].FindControl("TbItemsCode");
                    Label lblitemsName = (Label)GVUser.Rows[Convert.ToInt32(hf_Row.Value.ToString())].FindControl("LblitemsName");
                    Label LblgoodsUnit = (Label)GVUser.Rows[Convert.ToInt32(hf_Row.Value.ToString())].FindControl("LblgoodsUnit");
                    Label LblpartNameAlias = (Label)GVUser.Rows[Convert.ToInt32(hf_Row.Value.ToString())].FindControl("LblpartNameAlias"); 

                    tbitemsCode.Text = itemsCode;
                    lblitemsName.Text = itemsName;
                    lbitemsID.Text = itemsID;
                    LblgoodsUnit.Text = goodsUnit;
                    LblpartNameAlias.Text = material;

                }
                else
                {
                    Label lbitemsID = (Label)GVUser.FooterRow.FindControl("lbItemsID");
                    TextBox tbitemsCode = (TextBox)GVUser.FooterRow.FindControl("TbItemsCode");
                    Label lblitemsName = (Label)GVUser.FooterRow.FindControl("LblitemsName");
                    Label LblgoodsUnit = (Label)GVUser.FooterRow.FindControl("LblgoodsUnit");
                    Label LblpartNameAlias = (Label)GVUser.FooterRow.FindControl("LblpartNameAlias");


                    tbitemsCode.Text = itemsCode;
                    lblitemsName.Text = itemsName;
                    lbitemsID.Text = itemsID;
                    LblgoodsUnit.Text = goodsUnit;
                    LblpartNameAlias.Text = material;
                }
            }
        }

        protected void GVGoodslabelSearch_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                Label lblno = (Label)e.Row.FindControl("lblNo");
                lblno.Text = Convert.ToString(e.Row.RowIndex + 1 + (GVGoodslabelSearch.PageIndex * GVGoodslabelSearch.PageSize));
            }
        }

        protected void GVGoodslabelSearch_Sorting(object sender, GridViewSortEventArgs e)
        {

        }

        protected void GVGoodslabelSearch_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            DataTable dttemp = instMtItem.GetMtItems(new GenClassCommonParamList { ParamField1 = "a.whouseID", Operrator1 = "=", ParamFieldData1 = ddwhouse.SelectedValue, ParamField2 = ddSearch.SelectedValue, Operrator2 = "like", ParamFieldData2 = tbSearch.Text.Trim(), StepName = "LIST_SEARCH" });
            // this.setPaging(GVGoodslabelSearch, sender, e, dttemp);
        }
        #endregion GVSearch

        #region Button Event

        protected void btnClose1_Click(object sender, EventArgs e)
        {
            SetSession();
            ResponseRedirectPage();
        }
        protected void btnSave_Click(object sender, EventArgs e)
        {
            if (btnSave.Text.ToLower() == "save")
            {
                bool result = SaveUpdateHeader();
                if (result)
                {
                    SetView();
                }

            }
            else
            {
                btnSave.Text = "Save";
                SetEnableObject(true);
            }
        }



        protected void btnSearch_Click(object sender, EventArgs e)
        {
            if (ddwhouse.SelectedValue != "")
            {
                DataTable dttemp = new DataTable();
                dttemp = instMtItem.GetMtItems(new GenClassCommonParamList
                {
                    ParamField1 = "a.whouseID",
                    Operrator1 = "=",
                    ParamFieldData1 = ddwhouse.SelectedValue
                    ,
                    ParamField2 = ddSearch.SelectedValue,
                    Operrator2 = "like"
                    ,
                    ParamFieldData2 = tbSearch.Text.Trim(),
                    StepName = "LIST_REC"
                });
                BindDataGV(GVGoodslabelSearch, dttemp);
            }
            else
            {
                pgm.MessageBoxAjaxs(UpdatePanel1, "Please choose Werehouse field", false);
            }

        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            panelSearch.Visible = false;
        }
        #endregion

        #region gridviewEvent

        protected void GVAdmin_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            Control source = e.CommandSource as Control;
            GridViewRow row = source.NamingContainer as GridViewRow;
            //panelSearch.Visible = false;

            if (e.CommandName == "save")
            {
                Label lbItemsID = (Label)GVAdmin.FooterRow.FindControl("lblitemsID");
                TextBox tbLabelID = (TextBox)GVAdmin.FooterRow.FindControl("tbLabelID");
                TextBox tbQty = (TextBox)GVAdmin.FooterRow.FindControl("tbQty");
                TextBox tbblockName = (TextBox)GVAdmin.FooterRow.FindControl("tbblockName");
                TextBox tbweightQty = (TextBox)GVAdmin.FooterRow.FindControl("tbweightQty");
                Label lbWeightQty = (Label)GVAdmin.FooterRow.FindControl("lbWeightQty");

                DtGeneral item = new DtGeneral();
                item.ID = hf_ID.Value.ToString();
                item.itemsID = lbItemsID.Text;
                item.labelID = tbLabelID.Text;
                item.weightQty = tbweightQty.Text.Replace(",", ".");

                if (string.IsNullOrEmpty(tbweightQty.Text) && ((hf_projectTest.Value == "0") || hf_projectTest.Value == "False"))
                {
                    item.weightQty = "0";
                }
                else
                {
                    lbWeightQty.Visible = true;
                }
                string lblQTy = string.Empty;

                string mylabelID = tbLabelID.Text;
                if (!string.IsNullOrEmpty(mylabelID))
                {
                    GenClassCommonParamList pcp = new GenClassCommonParamList();
                    item.blockName = tbblockName.Text;
                    pcp.ManualQuery = " and a.labelID ='" + mylabelID + "' and a.slabel=0 ";
                    DataTable dtLabel = instLabel.GetDtlabel(pcp);
                    if (dtLabel.Rows.Count > 0)
                    {
                        //cek labelqty kosong
                        string labelQTY = dtLabel.Rows[0]["labelQTY"].ToString().Trim();
                        labelQTY = labelQTY.Replace(",", ".");
                        lblQTy = labelQTY;//.ToDouble(labelQTY);
                        string labelType = dtLabel.Rows[0]["labelType"].ToString();

                        if (labelType == "1")
                        {
                            item.issuedQty = Convert.ToString(0).Trim();
                        }
                        else
                        {
                            //Common
                            item.issuedQty = tbQty.Text;

                        }
                    }
                }


                bool Save = false;
                //lblQTy, item.issuedQty
                //hfParam1.Value = lblQTy.Trim();
                //hfParam2.Value = item.issuedQty.Trim();
                //bool cek = instGeneral.UpdateSubmitDtGeneral(getDtGeneral("CekLabelQtyAvailable")); //ganti manual aja, sebab di scalaar cuman membandingkan saja
                bool cek = false;
                decimal a = Convert.ToDecimal(lblQTy.Trim());
                decimal b = Convert.ToDecimal(item.issuedQty.Trim());
                if (a >= b)
                {
                    cek = true;
                }
                
                if (cek)
                {
                    item.InsertBy = hf_empno.Value;
                    item.StepName = "UPDATECommon"; //SPNamenya khusus
                    item.TableName = "DtIssuedWelderDet";
                    item.SPListBool = "0";
                    Save = instGeneral.UpdateSubmitDtGeneral(item);
                    if (Save)
                    {
                        lbWeightQty.Visible = false;
                        DataTable dtadminissdet = instissdet.GetDtIssuedDet(setParameter(tbldtLabel), "ListIssued2");
                        BindDataGV(GVAdmin, dtadminissdet);
                    }
                    else
                    {
                        pgm.MessageBoxAjaxs(UpdatePanel1, "Label Invalid or Label Qty Less Than Request !", false);
                    }

                }
                else
                {

                    pgm.MessageBoxAjaxs(UpdatePanel1, "Label Invalid or Label Qty Less Than Request !", false);
                }
            }
            else if (e.CommandName == "del")
            {
                Label LblReqDetID = ((Label)GVAdmin.Rows[row.RowIndex].FindControl("LblReqLabID"));
                Label lblQty = ((Label)GVAdmin.Rows[row.RowIndex].FindControl("lblQty"));
                Label LbllabelType = ((Label)GVAdmin.Rows[row.RowIndex].FindControl("LbllabelType"));
                Label tbLabelID = (Label)GVAdmin.Rows[row.RowIndex].FindControl("LbllabelID");


                //List<GenClassIdentity> mylist = new List<GenClassIdentity>();
                //GenClassIdentity pID = new GenClassIdentity();
                //pID.ID = LblReqDetID.Text.ToString();
                //mylist.Add(pID);

                DtGeneral item = new DtGeneral();
                item.StepName = "DELETECommon"; //SPNamenya khusus
                item.TableName = "DtIssuedWelderDet";
                item.SPListBool = "0";
                item.ID = LblReqDetID.Text.ToString();
                item.issuedQty = lblQty.Text.Replace(",", "."); 
                item.labelID = tbLabelID.Text;

                bool d = instGeneral.UpdateSubmitDtGeneral(item);//--instisswelderdet.deletespesial(hf_empno.Value.ToString(), mylist, lblQty.Text.Replace(",", "."), tbLabelID.Text);
                if (d == true)
                {
                    pgm.MessageBoxAjaxs(UpdatePanel1, pgm.msgDeleted(), false);
                    DataTable dtadminissdet = instissdet.GetDtIssuedDet(setParameter(tbldtLabel), "ListIssued2");
                    BindDataGV(GVAdmin, dtadminissdet);
                }
                else
                {
                    pgm.MessageBoxAjaxs(UpdatePanel1, pgm.msgDeletedFail(), false);
                }

            }
        }

        protected void GVAdmin_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                Label lblno = (Label)e.Row.FindControl("lblNo");
                lblno.Text = Convert.ToString(e.Row.RowIndex + 1 + (GVAdmin.PageIndex * GVAdmin.PageSize));

                if ((hf_modeView.Value.ToString().ToLower() == strTabComplete.ToLower()) || (hf_ro.Value.ToString().Trim() == "1"))
                {
                    GVAdmin.Columns[15].Visible = false;
                    GVAdmin.ShowFooter = false;
                }
                Label lblRemark = (Label)e.Row.FindControl("LblreturnMark");
                if (lblRemark.Text.Trim() == "RETURN")
                {
                    e.Row.BackColor = System.Drawing.Color.LightBlue;
                }
            }
        }

        protected void GVHistory_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                Label lblNo = (Label)e.Row.FindControl("lblNo");
                //  lblNo.Text = Convert.ToString((e.Row.RowIndex + 1) + (this.GVHistory.PageIndex * this.GVHistory.PageSize));

            }
        }

        protected void GVUser_RowUpdated(object sender, GridViewUpdatedEventArgs e)
        {
            GVUser.EditIndex = -1;
            this.GVUser.ShowFooter = true;
            BindDataGV(GVUser, GetDatatable(true, tbldtissuedDet, setParameter(tbldtissuedDet)));
        }

        protected void GV_Sorting(object sender, GridViewSortEventArgs e)
        {
            this.setSorting(((GridView)sender), sender, e, GetDatatable(false, tblmtItems, setParameter(tblmtItems)));

        }
        protected void GV_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            this.setPaging(((GridView)sender), sender, e, GetDatatable(false, tblmtItems, setParameter(tblmtItems)));

        }
        protected void GV_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            GridView gv = ((GridView)sender);
            string GVID = ((GridView)sender).ID.ToString();
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                Label lblNumber = (Label)e.Row.FindControl("lblNo");
                lblNumber.Text = Convert.ToString((e.Row.RowIndex + 1) + (((GridView)sender).PageIndex * ((GridView)sender).PageSize));

                if (hf_tabIndex.Value == "0" || hf_tabIndex.Value == "")
                {
                    Button btnDelete = (Button)e.Row.FindControl("btnDelete");
                    btnDelete.Visible = true;
                }
            }

            if (hf_tabIndex.Value == "0" || hf_tabIndex.Value == "")
            {

                gv.ShowFooter = true;
            }

        }
        protected void GridView_PreRender(object sender, EventArgs e)
        {
            GridView gv = (GridView)sender;
            if ((gv.Rows.Count > 0))
            {
                gv.UseAccessibleHeader = true;
                gv.HeaderRow.TableSection = TableRowSection.TableHeader;
            }
        }

        protected void GV_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            Control source = e.CommandSource as Control;
            GridViewRow row = source.NamingContainer as GridViewRow;

        }
        #endregion
        private bool SaveData(string itemSave)
        {
            bool result = false;

            return result;

        }

        #region SaveData

        #endregion SaveData

        #region apa

        protected void GVAdmin_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        protected void tbLabelID_TextChanged(object sender, EventArgs e)
        {

            TextBox tbLabelID = (TextBox)GVAdmin.FooterRow.FindControl("tbLabelID");
            Label lblLotNo = (Label)GVAdmin.FooterRow.FindControl("lblLotNo");
            Label lblitemsCode = (Label)GVAdmin.FooterRow.FindControl("lblitemsCode");
            Label lblitemsName = (Label)GVAdmin.FooterRow.FindControl("lblitemsName");
            Label lblitemsDesc = (Label)GVAdmin.FooterRow.FindControl("lblitemsDesc");
            Label LblitemsID = (Label)GVAdmin.FooterRow.FindControl("lblitemsID");
            Label lblQty = (Label)GVAdmin.FooterRow.FindControl("lblQty");
            TextBox tbQty = (TextBox)GVAdmin.FooterRow.FindControl("tbQty");
            TextBox tbblockName = (TextBox)GVAdmin.FooterRow.FindControl("tbblockName");
            TextBox tbweightQty = (TextBox)GVAdmin.FooterRow.FindControl("tbweightQty");


            string mylabelID = tbLabelID.Text;
            if (!string.IsNullOrEmpty(mylabelID))
            {
                GenClassCommonParamList pcp = new GenClassCommonParamList();
                pcp.ManualQuery = " and a.labelID ='" + mylabelID + "' and a.slabel=0 ";

                DataTable dtLabel = instLabel.GetDtlabel(pcp);
                if (dtLabel.Rows.Count > 0)
                {
                    lblLotNo.Text = dtLabel.Rows[0]["lotno"].ToString();
                    lblitemsCode.Text = dtLabel.Rows[0]["itemscode"].ToString();
                    lblitemsName.Text = dtLabel.Rows[0]["itemsname"].ToString();
                    lblitemsDesc.Text = dtLabel.Rows[0]["partDesc"].ToString();
                    LblitemsID.Text = dtLabel.Rows[0]["itemsid"].ToString();
                    lblQty.Text = dtLabel.Rows[0]["labelQTY"].ToString();
                    string labelType = dtLabel.Rows[0]["labelType"].ToString();

                    double qty = Convert.ToDouble(lblQty.Text.Replace(",", "."));
                    DtIssuedWelderDet item = new DtIssuedWelderDet();
                    item.issWelID = hf_ID.Value.ToString();
                    item.itemsID = LblitemsID.Text;
                    item.labelID = mylabelID;

                    if (labelType == "1")
                    {
                        tbQty.Visible = false;
                        tbblockName.Visible = true;
                    }
                    else
                    {
                        tbQty.Visible = true;
                        tbblockName.Visible = true;
                    }


                }
                else
                {
                    pgm.MessageBoxAjaxs(UpdatePanel1, "Label Invalid !", false);
                }

            }
        }

        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(hf_ID.Value.ToString()))
            {
                DataTable dtIssuedDet = instissdet.GetDtIssuedDet(setParameter(tbldtissuedDet));
                if (dtIssuedDet.Rows.Count > 0)
                {
                    if (!string.IsNullOrEmpty(tbTemp.Text))
                    {
                        bool submit = instGeneral.UpdateSubmitDtGeneral(getDtGeneral(strApprove));//instiss.Approve(item);
                        if (submit)
                        {

                            pgm.MessageBoxAjaxs(UpdatePanel1, "Issued Success !", false);
                            SetSession();
                            ResponseRedirectPage();
                        }
                        else
                        {
                            pgm.MessageBoxAjaxs(UpdatePanel1, "Issued Failed !", false);
                        }
                    }
                    else
                    {
                        pgm.MessageBoxAjaxs(UpdatePanel1, "Please Can't Be Empty !", false);

                    }

                }
                else
                {
                    pgm.MessageBoxAjaxs(UpdatePanel1, "Please Insert Label !", false);
                }
            }
            

        }

        private DtGeneral getDtGeneral(string ket)
        {
            DtGeneral dtg = new DtGeneral();
            dtg.ID = hf_ID.Value.Trim();
            dtg.InsertBy = hf_empno.Value.Trim();
            dtg.TableName = "DtIssued";
            dtg.SPListBool = "0";
            dtg.Comment = ddSuperior.SelectedValue.ToString();

            if (ket == "Request")
            {
                dtg.StepName = "SUBMIT";
            }
            else if (ket == "Propose")
            {
                dtg.StepName = "SUBMIT2";
            }
            else if (ket == "Store")
            {
                dtg.StepName = "SUBMIT3";
            }
            else if (ket == strApprove)
            {
                dtg.ID = hf_ID.Value.ToString();
                dtg.Comment = tbIssuedNote.Text;
                dtg.Temp = tbTemp.Text.Replace(",", ".");
                dtg.InsertBy = hf_empno.Value;
                dtg.StepName = strApprove;
            }
            return dtg;
        }

        private void setSuperior()
        {
            DataTable dtTaskID = new DataTable();
            DataTable dtAppinfo = new DataTable();
            DataTable dtSuperior = new DataTable();


            string strEmployee = string.Empty;
            strEmployee = hf_empno.Value;

            hf_taskID.Value = getTaskid("1");
            //hf_currenttaskID.Value = hf_taskID.Value;

            dtAppinfo = ica.GetApprovalGeneralInfo(hf_menuID.Value, hf_taskID.Value, hf_AppStep.Value, strEmployee);
            string curStep = (ica.GetNextApprovalStep(hf_menuID.Value, hf_taskID.Value, strEmployee, hf_AppStep.Value)).ToString();

            //if (hf_appTaskStep.Value == "1" && curStep == "1000000")
            //{
            //    hf_appTaskStep.Value = "2";
            //    hf_taskID.Value = getTaskid(hf_appTaskStep.Value.Trim());
            //    curStep = (ica.GetNextApprovalStep(hf_menuID.Value, hf_taskID.Value, strEmployee, "")).ToString();
            //}


            dtSuperior = ica.GetSuperiorByApprovalSetup(hf_menuID.Value, hf_taskID.Value, curStep, strEmployee);


            if (curStep == "1000000")
            {
                ddSuperior.Visible = false;
                lblSuperior.Text = "F I N I S H";
                lblSuperior.Visible = true;
            }
            if (dtSuperior.Rows.Count > 0)
            {
                lblSuperior.Visible = false;
                ddSuperior.Visible = true;
                pgc.setControlList(dtSuperior, "empno", "empname", ddSuperior);
            }
        }
        private string getTaskid(string apptaskstep)
        {
            string result = string.Empty;
            string error = string.Empty;

            //string deptId = lblDepartment.Text;
            //if (!string.IsNullOrEmpty(deptId))
            //{
            //    if (apptaskstep == "1")
            //    {
                    result = ica.GetTaskIDByTaskName(hf_menuID.Value, "MaterialRequest").ToString();

            //    }
            //    else if (apptaskstep == "2")
            //    {
            //        result = ica.GetTaskIDByTaskName(hf_menuID.Value, "ScrapStore").ToString();
            //    }
            //}

            return result;
        }

        protected void btnRequest_Click(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(hf_ID.Value.ToString()))
            {

                DataTable dtIssuedDet = instissdet.GetDtIssuedDet(setParameter(tbldtissuedDet));
               if (dtIssuedDet.Rows.Count > 0)
                {
                    bool submit = instGeneral.UpdateSubmitDtGeneral(getDtGeneral(btnRequest.Text)); // instiss.GetDtIssued // instissdet.Request(hf_ID.Value.ToString());
                    if (submit)
                    {

                        pgm.MessageBoxAjaxs(UpdatePanel1, "Request Success !", false);
                        SetSession();
                        ResponseRedirectPage();
                    }
                    else
                    {
                        pgm.MessageBoxAjaxs(UpdatePanel1, "Request Failed !", false);
                    }
                }
                else
                {
                    pgm.MessageBoxAjaxs(UpdatePanel1, pgm.msgSubmittedFail(), false);
                }
            }
        }


        protected void EdgoodsCode_SelectedIndexChanged(object sender, EventArgs e)
        {
            DataTable dttemp = new DataTable();
            TextBox tbSearch = (TextBox)GVUser.Rows[Convert.ToInt16(hf_Row.Value.Trim())].FindControl("EdgoodsCode");
            Label lblGoodsName = (Label)GVUser.Rows[Convert.ToInt16(hf_Row.Value.Trim())].FindControl("EdgoodsName");
            Label lbitemsID = (Label)GVUser.Rows[Convert.ToInt16(hf_Row.Value.Trim())].FindControl("EdItemsID");

            if (dttemp.Rows.Count > 0)
            {
                tbSearch.Text = dttemp.Rows[0]["goodsCode"].ToString();
                lblGoodsName.Text = dttemp.Rows[0]["goodsName"].ToString();
                lbitemsID.Text = dttemp.Rows[0]["itemsID"].ToString();
            }
        }

        protected void tbgoodsCode_SelectedIndexChanged(object sender, EventArgs e)
        {
            DataTable dttemp = new DataTable();
            TextBox tbSearch = (TextBox)GVUser.FooterRow.FindControl("tbgoodsCode");
            Label lblGoodsName = (Label)GVUser.FooterRow.FindControl("lblgoodsNameAdd");
            Label lblgoodsUnit = (Label)GVUser.FooterRow.FindControl("lblgoodsUnit");
            Label lblnowBudget = (Label)GVUser.FooterRow.FindControl("lblnowBudget");
            Label lbitemsID = (Label)GVUser.FooterRow.FindControl("lbItemsID");
            //dttemp = DAOStockistHeader.GetGoodsLabel(new PapuaBLCommonParamExt { ParamField1 = "a.stockistId", Operrator1 = "=", ParamFieldData1 = ddRequestTo.SelectedValue, ParamField2 = "A.GoodsCode", Operrator2 = "like", ParamFieldData2 = tbSearch.Text.Trim() });
            //dttemp = DAOStockistHeader.GetGoodsLabel(new PapuaBLCommonParamExt { ParamField1 = "a.stockistId", Operrator1 = "=", ParamFieldData1 = ddRequestTo.SelectedValue, ParamField2 = "A.GoodsCode", Operrator2 = "like", ParamFieldData2 = tbSearch.Text.Trim(), ManualQuery = " AND j.profit_ctr='" + hf_profitCtr.Value + "'", StepName = "LIST_SEARCH" });

            if (dttemp.Rows.Count > 0)
            {
                tbSearch.Text = dttemp.Rows[0]["goodsCode"].ToString();
                lblGoodsName.Text = dttemp.Rows[0]["goodsName"].ToString();
                lbitemsID.Text = dttemp.Rows[0]["itemsID"].ToString();
                lblgoodsUnit.Text = dttemp.Rows[0]["goodsUnit"].ToString();
                lblnowBudget.Text = dttemp.Rows[0]["nowBudget"].ToString();
            }
        }
        protected void Btnupdate_Click(object sender, EventArgs e)
        {
            SetEnableObject(true);
            btnSave.Visible = true;
            Btnupdate.Visible = false;
            btnCancel.Visible = true;
        }
        protected void btnCancel_Click1(object sender, EventArgs e)
        {
            SetEnableObject(true);
            btnSave.Visible = false;
            Btnupdate.Visible = true;
            btnCancel.Visible = false;

        }

        #endregion

        protected void btnClose_Click(object sender, EventArgs e)
        {
            //SetSession();
            //SetStringSession(hf_tabIndex.Value, "sesTab" + hf_empno.Value.Trim() + hf_loginTime.Value.Trim() + hf_menuID.Value.Trim());

            //Response.Redirect("CandidateApproval.aspx?",false);
            this.ResponseRedirectPage();

        }

        protected void gvHistoryApproval_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            string GVID = ((GridView)sender).ID.ToString();
            //if (e.Row.RowType == DataControlRowType.DataRow)
            //{
            //    Label lblNumber = (Label)e.Row.FindControl("lblNo");
            //    lblNumber.Text = Convert.ToString((e.Row.RowIndex + 1) + (((GridView)sender).PageIndex * ((GridView)sender).PageSize));

            //}
            if (e.Row.RowType == DataControlRowType.Header)
            { e.Row.TableSection = TableRowSection.TableHeader; }

            if (e.Row.RowType == DataControlRowType.Pager)
            { e.Row.TableSection = TableRowSection.TableFooter; }
            //TableFooterRow tfr = new TableFooterRow();
            //tfr.TableSection = TableRowSection.TableFooter; // ADD THIS LINE

        }
        protected void ScanQR_Click(object sender, EventArgs e)
        {

        }

        [WebMethod()]
        public static string ScanData(string param)
        {
            string myRes = "[SUCCESS]"; //hf_ID.Value.Trim();
            string[] paramArray = param.Split(',');
            List<List<string>> li = new List<List<string>>();
            //List<string> mylist = new List<string>();
            string myID = paramArray[0];
            string kode2 = paramArray[1];
            string partno = paramArray[42];
            string runningno = paramArray[43]; //running no
            myRes += "|" + paramArray[2];

            string cb = paramArray.Length.ToString();

            for (int i = 4; i < paramArray.Length - 10; i++)
            {
                // mylist.Add(paramArray[i] + "," + paramArray[i + 1]  + "," + paramArray[i + 2] + "," + paramArray[i + 3]);
                List<string> mylist = new List<string>();
                //mylist.Add(myre);
                mylist.Add(myID); // ID
                mylist.Add(partno); // partno
                mylist.Add(paramArray[i]); // batchno
                mylist.Add(paramArray[i + 1]); // qty
                mylist.Add(runningno); //running no
                mylist.Add("kode1");
                mylist.Add(kode2); //kode 2
                mylist.Add(kode2); //kode 3
                mylist.Add("expdate");

                i = i + 3;
                li.Add(mylist);
            }
            //li.Add(mylist);

            PartRequestIssuedDetail myPage = new PartRequestIssuedDetail();
            int savedet = myPage.instissdet.SaveDtUploadIssued(li);
            if (savedet > 0)
            {
                myRes += "|" + " " + "Thank You";
                //myPage.BindForm();
            }
            else
            {
                myRes = "[FAILED]| Register PartNo First";
            }

            return myRes;


        }



    }

}